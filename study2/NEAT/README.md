# NEAT (Question Answering Version)

* `collectAttentionResults.py`
* `Rhistory_joined.R`
* `Rhistory.R`
* `train_attention_7DailyMail_500_Recurrent_Record_Save_9b.py` Train attention module
* `train_attention_7DailyMail_500_Recurrent_Record_Save_9b_RunOnSample_All.py` Run on a sample from the Deepmind Corpus.
* `train_attention_7DailyMail_500_Recurrent_Record_Save_9b_RunOnSample.py`
* `train_attention_7DailyMail_500_Recurrent_Record_Save_9b_RunOnTexts_All.py` Run on the texts from the eyetracking study.
* `train_attention_7DailyMail_500_Recurrent_Record_Save_9b_RunOnTexts.py`
* `train_qa_dropout_mask_relabling_attrain_decay_Chosen_Hybrid.py` Pretrain QA module.
* `train_qa_dropout_mask_relabling_Bugfix.py`
* `uniteExptFiles.py`
* `uniteFiles.py` Prepare corpus
* `uniteSampleFiles.py`
* `uniteSample.py`
* `vocabularies`
