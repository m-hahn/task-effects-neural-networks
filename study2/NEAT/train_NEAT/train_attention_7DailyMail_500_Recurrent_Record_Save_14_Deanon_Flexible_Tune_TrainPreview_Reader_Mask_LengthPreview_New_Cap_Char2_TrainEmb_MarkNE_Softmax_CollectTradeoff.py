import glob
import subprocess
files = glob.glob(f"BASE/STUDY2/ACCURACIES/*{__file__.replace('_CollectTradeoff.py', '.py')}_*.txt")
files += glob.glob(f"BASE/STUDY2/ACCURACIES/*{__file__.replace('_CollectTradeoff.py', '')}_Baseline.py*.txt")
files += glob.glob(f"BASE/STUDY2/ACCURACIES/*{__file__.replace('_CollectTradeoff.py', '')}_Baseline2.py*.txt")
files += glob.glob(f"BASE/STUDY2/ACCURACIES/*{__file__.replace('_CollectTradeoff.py', '')}_Continue.py*.txt")
header = None
print(files)
#with open("BASE/STUDY2/ACCURACIES/SUMMARY_{__file__}.tsv", "w") as outFile:
with open(f"logs/SUMMARY_{__file__}.tsv", "w") as outFile:
 print("\t".join([str(q) for q in ["myID", "LAMBDA", "feedPreview", "learning_rate", "learning_rate_attention", "lengthUntil", "previewLength", "Tradeoff", "Condition", "Accuracy", "Fixations", "NumEpochs", "script"]]), file=outFile)
 for f in files:
 # try:
   with open(f, "r") as inFile:
       args = next(inFile).strip()
       accuracies = [float(x) for x in next(inFile).strip().strip()[1:-1].split(", ")]
       next(inFile)
       rewards = [float(x) for x in next(inFile).strip()[1:-1].split(", ") ]
       next(inFile)
       tradeoff = next(inFile).strip()
   print(args)
   if  True:
#          print(tradeoff, len(rewards))
          print(f)
          modelNumber = f[f.rfind("/")+1:].replace("accuracy_", "").replace(".txt", ".ckpt")
#          quit()
 #         modelNumber = f[f.rfind("_")+1:][:-4]
          print(modelNumber)

          tradeoff = tradeoff.split("\t")
          print(tradeoff)
          tradeoff[1] = tradeoff[1].strip().split(" ")
          tradeoff[2] = tradeoff[2].strip().split(" ")
          args = dict([x.split("=") for x in args.replace("Namespace(", "")[:-1].split(", ")])
          print(args)
          script = f.split("/")[-1]
          script = script[:script.rfind("_")][-50:]
          print(script)
          #quit()
          print("\t".join([str(q) for q in [args["myID"], args["LAMBDA"], args["feedPreview"], args["learning_rate"], args["learning_rate_attention"], args["lengthUntil"], args["previewLength"], float(tradeoff[0]), "NoPreview", tradeoff[1][1], tradeoff[2][1], len(accuracies), script]]), file=outFile)
          print("\t".join([str(q) for q in [args["myID"], args["LAMBDA"], args["feedPreview"], args["learning_rate"], args["learning_rate_attention"], args["lengthUntil"], args["previewLength"], float(tradeoff[0]), "Preview", tradeoff[1][2], tradeoff[2][2], len(accuracies), script]]), file=outFile)

