import random
import argparse

parser = argparse.ArgumentParser()


parser.add_argument('--corpus', type=str, default="joint") # Which corpus to train on? Note: when running the experiments, this was previously set to 'cnn' but then ignored downstream, i.e. training was on the joint set
parser.add_argument('--batchSize', type=int, default=random.choice([32]))
parser.add_argument('--learning_rate', type=float, default=random.choice([0.05]))
parser.add_argument('--glove', type=bool, default=True)
parser.add_argument('--dropout', type=float, default=random.choice([0.3]))
parser.add_argument('--lr_decay', type=float, default=random.choice([0.5]))
parser.add_argument('--myID', type=int, default=random.randint(1000,100000000))

args = parser.parse_args()



vocabulary = [x.split("\t") for x in open(f"vocabularies/cnn_nonAnonymized.txt", "r").read().strip().split("\n")]
itos = [x[1] for x in vocabulary]
stoi = dict([(x[1], int(x[0])) for x in vocabulary])

def unique(l):
    s = set()
    r = []
    for x in l:
      if x not in s:
        s.add(x)
        r.append(x)
    return r

answerDistribution = [0, [0 for _ in range(600)]]


#########################################

# Prepare Texts
def permuteEntitiesInQ(result):
    result["answer"] = result["answer"].strip("#") # due to some upstream segmentation problem, this has an initial '#' in one text

    entities = unique([x for x in result["text"]+result["question"]+[result["answer"]] if x.startswith("@entity")])
    entities_mapping = dict(zip(entities, [f"@entity{x}" for x in range(len(entities))]))
    assert len(entities_mapping) == len(entities)
    result["text"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["text"]]
    result["question"] = [entities_mapping[x] if x.startswith("@entity") else x for x in result["question"]]
    result["answer"] = entities_mapping[result["answer"]]
    mapping_new = []
    for x in result["mapping"]:
       q, w = x[:x.index(":")], x[x.index(":")+1:]
       if q not in entities_mapping: # Not all entities actually turn up in the text
#         print("Warning 46", x, result["mapping"], entities)
         continue
       mapping_new.append(entities_mapping[q]+":"+w)
    result["mapping"] = mapping_new

def deanonymize(entityMapping, text):
   text_ = []
   text__ = []
   for x in text:
     if x in entityMapping:
        for z in entityMapping[x].split(" "):
          text_.append(z)
          text__.append(x)
     else:
        text_.append(x)
        text__.append(x)
   return text_, text__

def makeEntitiesMask(entityMapping, text):
   text_ = []
   for x in text:
     if x in entityMapping:
        text_ += [0 for _ in entityMapping[x].split(" ")]
     else:
        text_.append(-1e100)
   return text_


def processQuestion(question, permuteEntities=True):
    question = question.split("##########")
    question = [x.translate(x.maketrans("—”“»…‘’!–", "-\"\"\".''!-" ))  for x in question]

    result = {"text" : question[1].split(" "), "question" : question[2].split(" "), "answer" : question[3], "mapping" : question[4].split("@@@@@")}
    if permuteEntities:
       permuteEntitiesInQ(result)

    TEXT = result["text"]
    QUESTION = result["question"]
    ANSWER = result["answer"]
    MAPPING = result["mapping"]

    entityList =        ([(lambda q: (q[0]))(x.split(":")) for x in MAPPING])
    entityMapping = dict([(lambda q: (q[0], capitalize(q[1].strip())))(x.split(":")) for x in MAPPING])
    entityList = [x[0] for x in entityMapping.items()]



    # Now prepare the result
    result = {}
    result["text"], result["text_NEs"] = deanonymize(entityMapping, TEXT)
    result["question"], result["question_NEs"] = deanonymize(entityMapping, QUESTION)
    result["answer"], _ = deanonymize(entityMapping, ANSWER)
    result["answer_index"] = entityList.index(ANSWER)
    result["answer_ID"] = ANSWER
    result["all_entities"] = [entityMapping[y].split(" ") for y in entityList]
    result["text_mask"] = makeEntitiesMask(entityMapping, TEXT)

    return result

def loadQACorpus(corpus, partition, batchSize, permuteEntities=True):
   assert corpus == "joint"
   assert partition in ["test", "training", "validation"]
   with open(f"BASE/STUDY2/DEEPMIND_CORPUS_CHO/cnn_{partition}.txt", "r") as inFile1:
    with open(f"BASE/STUDY2/DEEPMIND_CORPUS_CHO/dailymail_{partition}.txt", "r") as inFile2:
     while True:
        buff = []
        print("Filling buffer...")
        for _ in range(10*args.batchSize):
         try:
           for x in [inFile1, inFile2]:
             buff.append(processQuestion(next(x).strip(), permuteEntities=permuteEntities))
         except StopIteration:
            break
        if len(buff) == 0:
           break
        buff = sorted(buff, key=lambda x:len(x["text"]))
        partitions = [buff[i*args.batchSize:(i+1)*args.batchSize] for i in range(21)]
        random.shuffle(partitions)
        for partition in partitions:
           if len(partition) > 0:
               random.shuffle(partition) # ensure there is no confond between condition and length
               yield partition

#######################################################

OOV = 2
SKIPPED = 1
PAD = 0
PLACEHOLDER = 3

#training = 

def numerify_NE(token):
   if token.startswith("@ent"):
      return int(token.replace("@entity", "")) + 10
   else:
     return 0


def numerify(token):
   if token == "@placeholder":
      return PLACEHOLDER
   elif token not in stoi or stoi[token] >= 50000:
      return OOV
   else:
      return stoi[token]+4

import torch





word_embeddings = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 200).cuda()




if args.glove:
 with open("BASE/glove/glove.6B.200d.txt", "r") as inFile:
  print("Loading embeddings")
  counter = 0
  for line in inFile:
    counter += 1
    line = next(inFile).strip().split(" ")
    word = line[0]
    if word in stoi and stoi[word] < 50000:
       #print(stoi[word])
       embedding = torch.FloatTensor([float(x) for x in line[1:]])
       word_embeddings.weight.data[stoi[word]+4] = embedding
       # print(counter, word)
    if counter > 100000:
      break
  print("Done loading embeddings")
 

text_reader = torch.nn.LSTM(200, 128, 1, bidirectional=True).cuda()
question_reader = torch.nn.LSTM(200, 128, 1, bidirectional=True).cuda()


U = torch.nn.Parameter(torch.Tensor(256,256).cuda())
U.data.fill_(0)


output = torch.nn.Linear(256, 600 + 4).cuda()

input_dropout = torch.nn.Dropout(args.dropout)

nllLoss = torch.nn.NLLLoss()
crossEntropy = torch.nn.CrossEntropyLoss()

entities_embedding = torch.nn.Embedding(600, 200).cuda()


components_lm = [word_embeddings, text_reader, question_reader, output, entities_embedding]


def parameters():
 for c in components_lm:
   for param in c.parameters():
      yield param
 yield U

#parameters_lm_cached = [x for x in parameters_lm()]



optimizer = torch.optim.SGD(parameters(), lr = args.learning_rate)




def getEntities(x):
    return set([int(y.replace("@entity", "")) for y in x["text_NEs"] + x["question_NEs"] + [x["answer_ID"]] if y.startswith("@entity")])

# the overall forward pass
def forward(batch, calculateAccuracy=False):
  
    entities_IDs_numerical =  [[numerify_NE(y) if calculateAccuracy or random.random() > 0.1 else SKIPPED for y in x["text_NEs"]] for x in batch]
    texts = [[numerify(y) if calculateAccuracy or random.random() > 0.1 else SKIPPED for y in x["text"]] for x in batch] # [:500]
#    print([len(texts[0]), len(texts[1])])
    questions = [[numerify(y) for y in x["question"]] for x in batch]
    answers_IDs = [int(x["answer_ID"].replace("@entity", "")) for x in batch]
    answers = [int(x["answer_index"]) for x in batch]
    #texts_mask = [x["text_mask"] for x in batch]
    # Process the answer options
    #answers_flattened = []
    #all_entities = [[[numerify(y) for y in z] for z in x["all_entities"]] for x in batch]
#    number_of_entities = sum([len(x) for x in all_entities])
 #   all_entities_flattened = []
    #mask_flattened = torch.zeros(len(batch), number_of_entities) - 1e100
#    for i, x in enumerate(all_entities):
 #      for j, y in enumerate(x):
          #mask_flattened[i, len(all_entities_flattened)] = 0
 #         if j == answers[i]:
#             answers_flattened.append(len(all_entities_flattened))
          #all_entities_flattened.append(y)
    #mask_flattened = mask_flattened.cuda()
    #answers_flattened = torch.LongTensor(answers_flattened).cuda()
#    entities_length = max([max([len(q) for q in z]) for z in all_entities])
 #   for text in all_entities:
  #     for entity in text:
   #       while len(entity) < entities_length:
    #         entity.append(PAD)
    #print(all_entities)
    #print(entities_length)
#    entities_number = max([len(q) for q in all_entities])
    #print(entities_number, min([len(q) for q in all_entities]))
    #print(len(all_entities_flattened))
    text_length = max([len(x) for x in texts])
    question_length = max([len(x) for x in questions])+2
#    for text in texts_mask:
 #      while len(text) < text_length:
  #        text.append(1e-100)
    for text in entities_IDs_numerical:
       while len(text) < text_length:
          text.append(0)
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)
    for question in questions:
       while len(question) < question_length:
          question.append(PAD)

    #texts_mask = torch.FloatTensor(texts_mask).cuda()

    # anonymized
    entitiesPerText = [getEntities(x) for x in batch]
    MASK = torch.FloatTensor(len(batch), 600+4)
    MASK.fill_(float("-inf"))
    for i in range(len(batch)):
       for j in entitiesPerText[i]:
          MASK[i,j] = 0
    MASK = MASK.cuda()
#    texts = [[random.randint(1,10) for _ in range(args.batchSize)] for j in range(args.batchSize)]
#    questions = [[j, PLACEHOLDER] for j in range(args.batchSize)] # need to add j
#    answers = [texts[j][j] for j in range(args.batchSize)]
#    data = list(zip(texts, questions, answers))
##    print(min(answers), max(answers))
#    random.shuffle(data)
#    texts, questions, answers = zip(*data)
##    print(answers)
#    questions_raw = questions
    
    entities_IDs_numerical  = torch.LongTensor(entities_IDs_numerical).cuda().transpose(0,1)
     
    texts =  torch.LongTensor(texts).cuda().transpose(0,1)
    questions = torch.LongTensor(questions).cuda().transpose(0,1)
    answers = torch.LongTensor(answers_IDs).cuda()
#    all_entities_flattened = torch.LongTensor(all_entities_flattened).cuda().transpose(0,1)
#    print(texts.max(), texts.min())
    texts = word_embeddings(texts)
    entities_markup = entities_embedding(entities_IDs_numerical)
    texts = texts + entities_markup
    questions = word_embeddings(questions)
#    all_entities_flattened = word_embeddings(all_entities_flattened)
    output_text, _ = text_reader(texts)
    if not calculateAccuracy and args.dropout > 0:
       texts = input_dropout(texts)
       questions = input_dropout(questions)
    _, question_representations = question_reader(questions)
    #print(output_text.size())
    question_representations = question_representations[0].transpose(0,1).contiguous().view(-1, 256)
    #print(question_representations.size())

    part1 = torch.matmul(output_text, U)
    #print("PART1", part1.size(), "Q", question_representations.size())
    question_representations = question_representations.unsqueeze(2) # torch.transpose(question_representations, 0, 1).unsqueeze(1)
    part1 = part1.unsqueeze(2)
    #print("PART1", part1.size(), "Q", question_representations.size())
    attention = torch.matmul(part1, question_representations) # 
    attention = attention.squeeze(2).squeeze(2)
    attention = torch.nn.functional.softmax(attention, dim=0)
    if random.random() < 0.01:
       attention0 = attention[:,0].cpu()
       for i in range(len(batch[0]["text"])):
          print(i, "\t", batch[0]["text"][i], "\t", float(attention0[i]), "\t", batch[0]["answer"], "\t", " ".join(batch[0]["question"]))
    text_representation =  output((attention.unsqueeze(2) * output_text).sum(dim=0)) + MASK
    loss = crossEntropy(text_representation, answers)
    if calculateAccuracy:
       prediction = text_representation.argmax(dim=1)
       return loss, float((prediction == answers).float().mean())
    else:
       return loss

args.clip_type = random.choice([2, "inf", "None"])
args.clip_bound = random.choice([2, 5, 10, 15])

def backward(loss):
   optimizer.zero_grad()
   loss.backward()
   if args.clip_type is not "None":
#      print("NROM MAX", max(p.grad.detach().abs().max().cpu() for p in parameters()))
      torch.nn.utils.clip_grad_norm_(parameters(), args.clip_bound, norm_type=args.clip_type)
   optimizer.step()

import time

expectedEntropy = 0

learning_rate=args.learning_rate

devLosses = []
lossRunningAverage = 6.4
devAccuracies = []
noImprovement = 0

FROM_ATTENTION_WEIGHT = 1.0

for epoch in range(10000):


  validLoss = []
  examplesNumber = 0
  validAccuracy = []
  for batch in  loadQACorpus(args.corpus, "validation", args.batchSize, permuteEntities=True):
   with torch.no_grad():
     loss, accuracy = forward(batch, calculateAccuracy = True)
     print("VALID", loss, examplesNumber)
   validLoss.append(float(loss)*len(batch))
   examplesNumber += len(batch)
   validAccuracy.append(float(accuracy)*len(batch))
  devLosses.append(sum(validLoss)/examplesNumber)
  devAccuracies.append(sum(validAccuracy)/examplesNumber)
  if len(devAccuracies) >1 and devAccuracies[-1] < devAccuracies[-2]:
     learning_rate *= args.lr_decay
     optimizer = torch.optim.SGD(parameters(), lr = learning_rate)
     noImprovement += 1
  elif len(devAccuracies) > 1 and devAccuracies[-1] >= max(devAccuracies):
#     assert False
     torch.save({"devAccuracies" : devAccuracies, "devLosses" : devLosses, "args" : args, "components" : [x.state_dict() for x in components_lm], "U" : U.data, "learning_rate" : learning_rate}, f"BASE/CHECKPOINTS/{__file__}_{args.myID}.ckpt")

     noImprovement = 0
  print(f"BASE/STUDY2/ACCURACIES/accuracy_{__file__}_{args.myID}.txt")
  with open(f"BASE/STUDY2/ACCURACIES/accuracy_{__file__}_{args.myID}.txt", "w") as outFile:
     print(args, file=outFile)
     print(devAccuracies, file=outFile)
     print(devLosses, file=outFile)

  if noImprovement > 10:
    print("End training, no improvement for 10 epochs")
    break
  words = 0
  timeStart = time.time()
  for batch in loadQACorpus(args.corpus, "training", args.batchSize, permuteEntities=True):
    loss = forward(batch)
    backward(loss)
    lossRunningAverage = 0.99 *lossRunningAverage + (1-0.99) * float(loss)
    words += len(batch)
    print(devAccuracies)
    print(devLosses)
    print(float(loss), lossRunningAverage, expectedEntropy, args, __file__)
    print(words/(time.time()-timeStart), "questions per second")
#    if expectedEntropy == 0 or (answerDistribution[0] < 20000 and random.random() < 0.05):
#       dist = torch.FloatTensor(answerDistribution[1])
#       dist = dist / dist.sum()
#       expectedEntropy = float(-(dist*(dist+1e-10).log()).sum())
      
print(devAccuracies)
print(devLosses)

