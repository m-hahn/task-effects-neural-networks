library(tidyr)
library(dplyr)
SCR = Sys.getenv("SCR")
SCR="~/scr/"
human = read.csv(paste("../experiment_data/processed_data/mappingWithHuman.tsv", sep=""), sep="\t") %>% group_by(TextFileName, HumanPosition, AnonymizedPosition, AnonymizedToken, OriginalToken, ExperimentToken, Condition, TextNo, JointPosition) %>% summarise(HumanFixationRate=mean(fp>0, na.rm=TRUE))
model = read.csv(paste("BASE/STUDY2/OUTPUTS_TEXTS/merged.tsv", sep=""), sep="\t")
model = model %>% mutate(FixationProb = 1/(1+exp(-AttentionLogit)))
model_ = model %>% group_by(Text, Position, Word, IsCorrectAnswer, Condition, IsOOV, IsNamedEntity, LogWordFreq) %>% summarise(FixationProb = mean(FixationProb))
joint = merge(human, model_ %>% mutate(TextFileName=Text, AnonymizedPosition = Position), by=c("Condition", "TextFileName", "AnonymizedPosition"))


text = joint[joint$TextFileName == "TE-2" & joint$Condition == "Preview" & joint$JointPosition < 175,]
text = text[order(text$JointPosition),]
text = text[text$ExperimentToken != "",]

low = min(text$FixationProb, na.rm=TRUE)                           
high=0.7 #max(text$FixationProb, na.rm=TRUE)                            

sink("output/heatmap-human-preview.tex")
cat("\\definecolor{color0}{rgb}{0,0.3,1}")
cat("\\definecolor{color1}{rgb}{0.1,0.3,1}")
cat("\\definecolor{color2}{rgb}{0.2,0.3,1}")
cat("\\definecolor{color3}{rgb}{0.3,0.3,1}")
cat("\\definecolor{color4}{rgb}{0.4,0.3,1}")
cat("\\definecolor{color5}{rgb}{0.5,0.3,1}")
cat("\\definecolor{color6}{rgb}{0.6,0.3,1}")
cat("\\definecolor{color7}{rgb}{0.7,0.3,1}")
cat("\\definecolor{color8}{rgb}{0.8,0.3,1}")
cat("\\definecolor{color9}{rgb}{0.9,0.3,1}")
cat("\\definecolor{color10}{rgb}{1,0.3,1}")
cat("\\definecolor{color11}{rgb}{1,0.3,0.9}")
cat("\\definecolor{color12}{rgb}{1,0.3,0.8}")
cat("\\definecolor{color13}{rgb}{1,0.3,0.7}")
cat("\\definecolor{color14}{rgb}{1,0.3,0.6}")
cat("\\definecolor{color15}{rgb}{1,0.3,0.5}")
cat("\\definecolor{color16}{rgb}{1,0.3,0.4}")
cat("\\definecolor{color17}{rgb}{1,0.3,0.3}")
cat("\\definecolor{color18}{rgb}{1,0.3,0.2}")
cat("\\definecolor{color19}{rgb}{1,0.3,0.1}")
cat("\\definecolor{color20}{rgb}{1,0.3,0}")
cat("\\definecolor{none}{rgb}{0.99,0.99,0.99}","\n",sep="")                     
for(i in (1:nrow(text))) {                              
   colorID = min(18, max(3, floor((text$HumanFixationRate[i] - low) * 20 / (high - low))))                  
   cat(" \\colorbox{color", colorID,     "}{",as.character(text$ExperimentToken[i]),"}",sep="")                     
   if(i %% 10 == 0) {                           
     cat("\n")                            
   }                                
} 
sink()

