* [Descriptive Statistics](basic_stats)
* [Mixed-Effects Models](mixed_models), [without residualization](mixed_models_noResid)
* [Forward Model Selection](select_interactions), [without residualziation](select_interactions_NoResid)
* [Visualization](visualization-clip-avg)
