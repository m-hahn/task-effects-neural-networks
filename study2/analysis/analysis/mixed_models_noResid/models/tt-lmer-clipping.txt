
\begin{table}$
\begin{center}$
\begin{tabular}${l c}$
\hline
 & Model 1 \\
\hline
(Intercept)                                             & 268.82 &  (6.43) &  $^{***}$ \\
PositionText                                  & -5.95 &  (0.85) &  $^{***}$  \\
Condition                                & 55.39 & (12.76) &  $^{***}$  \\
LogWordFreq                                    & -11.71 &  (1.29) &  $^{***}$ \\
IsNamedEntity                                  & 5.30 &  (3.25) &           \\
IsCorrectAnswer                                & 35.48 & (12.30) &  $^{**}$   \\
Surprisal                                      & 7.42 &  (0.79) &  $^{***}$   \\
WordLength                          & 26.93 &  (1.11) &  $^{***}$  \\
Condition:IsCorrectAnswer       & -62.53 &  (9.83) &  $^{***}$ \\
Condition:LogWordFreq           & -7.48 &  (1.59) &  $^{***}$  \\
LogWordFreq:IsNamedEntity             & -16.98 &  (2.69) &  $^{***}$ \\
PositionText:IsCorrectAnswer         & -42.60 & (10.04) &  $^{***}$ \\
IsCorrectAnswer:WordLength & -5.51 &  (8.68) &          \\
Condition:WordLength & 7.00 &  (1.48) &  $^{***}$   \\
PositionText:LogWordFreq             & 4.28 &  (0.83) &  $^{***}$   \\
LogWordFreq:IsCorrectAnswer           & 3.19 &  (9.37) &           \\
IsCorrectAnswer:Surprisal             & -7.82 &  (5.16) &          \\
Condition:IsNamedEntity         & 1.39 &  (3.65) &           \\
LogWordFreq:WordLength     & 0.15 &  (0.93) &           \\
\hline
AIC                                                     & 791134.05               \\
BIC                                                     & 791332.67               \\
Log Likelihood                                          & -395545.02              \\
Num. obs.                                               & 61599                   \\
Num. groups: tokenID                                    & 4984                    \\
Num. groups: Participant                                & 22                      \\
Var: tokenID (Intercept)                                & 1568.70                 \\
Var: Participant (Intercept)                            & 879.13                  \\
Var: Residual                                           & 21022.50                \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001;  &  $^{**}$p<0.01;  &  $^{*}$p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
