
\begin{table}$
\begin{center}$
\begin{tabular}${l c}$
\hline
 & Model 1 \\
\hline
(Intercept)                                             & 5.33 & (0.03) &  $^{***}$  \\
PositionText                                  & -0.00 & (0.00) &         \\
Condition                                & 0.16 & (0.05) &  $^{**}$   \\
LogWordFreq                                    & -0.03 & (0.00) &  $^{***}$ \\
IsNamedEntity                                  & -0.04 & (0.01) &  $^{***}$ \\
IsCorrectAnswer                                & -0.08 & (0.04) &  $^{*}$   \\
Surprisal                                      & 0.02 & (0.00) &  $^{***}$  \\
WordLength                          & 0.07 & (0.00) &  $^{***}$  \\
Condition:WordLength & 0.02 & (0.00) &  $^{***}$  \\
LogWordFreq:IsNamedEntity             & -0.05 & (0.01) &  $^{***}$ \\
LogWordFreq:WordLength     & -0.00 & (0.00) &         \\
Condition:LogWordFreq           & -0.01 & (0.00) &  $^{**}$  \\
LogWordFreq:IsCorrectAnswer           & -0.08 & (0.03) &  $^{**}$  \\
PositionText:IsCorrectAnswer         & -0.06 & (0.03) &  $^{*}$   \\
IsCorrectAnswer:Surprisal             & -0.02 & (0.02) &         \\
PositionText:LogWordFreq             & 0.00 & (0.00) &          \\
LogWordFreq:Surprisal                 & 0.00 & (0.00) &          \\
\hline
AIC                                                     & 48246.37              \\
BIC                                                     & 48421.22              \\
Log Likelihood                                          & -24103.18             \\
Num. obs.                                               & 46291                 \\
Num. groups: tokenID                                    & 4945                  \\
Num. groups: Participant                                & 22                    \\
Var: tokenID (Intercept)                                & 0.01                  \\
Var: Participant (Intercept)                            & 0.01                  \\
Var: Residual                                           & 0.16                  \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001;  &  $^{**}$p<0.01;  &  $^{*}$p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
