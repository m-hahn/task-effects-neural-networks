# For first fixation duration and first pass time, no trials in which the region is skipped on first-
# pass reading (i.e., when first fixation duration is zero) were included in the analysis. For total time,
# only trials with a non-zero total time were included in the analysis.


source("../prettyPrint.R")

library(tidyr)
library(dplyr)

mapping_surprisals = read.csv("../../experiment_data/processed_data//mappingSurprisals.tsv", sep="\t", quote="@")
mapping_surprisals$MappingLineNum = (1:nrow(mapping_surprisals))-1
joint = mapping_surprisals
human = read.csv("../../experiment_data/processed_data//mappingWithHuman.tsv", sep="\t")

humanWordFreq = read.csv("../../experiment_data/processed_data/mappingNonAnonymizedWordFreq.tsv", sep="\t", quote="^")

human = merge(human, humanWordFreq, by=c("TextFileName", "HumanPosition", "AnonymizedPosition"), all.x=TRUE)
print(mean(as.character(human$OriginalToken.x) == as.character(human$OriginalToken.y), na.rm=TRUE))
human$OriginalToken = human$OriginalToken.x
human$OriginalToken.x = NULL
human$OriginalToken.y = NULL


human$LogWordFreq = log(human$NonAnonymizedWordFreq+1)


# EXCLUDED PARTICIPANTS
human = human[!(human$Participant %in% c("N_3", "N_9")),]



joint = merge(human, joint, by=c("TextFileName", "HumanPosition", "AnonymizedPosition")) #, "OriginalToken"))

mean(as.character(joint$OriginalToken.x) == as.character(joint$OriginalToken.y), na.rm=TRUE)
joint$OriginalToken = joint$OriginalToken.y
joint$OriginalToken.x = NULL
joint$OriginalToken.y = NULL


library(lme4)

external = read.csv("../../experiment_data/processed_data/mappingWithExternal.tsv", sep="\t") %>% select(TextFileName, JointPosition, AnonymizedToken, ExperimentTokenLength, WordFreq, IsNamedEntity, IsCorrectAnswer) %>% mutate(WordFreq=NULL) # this is the wrong WordFreq --> it is the anonymized one!
joint = merge(joint, external, by=c("TextFileName", "JointPosition", "AnonymizedToken"))



joint$tokenID = as.factor(paste0(joint$TextFileName, joint$JointPosition))


library(brms)
joint = joint %>% filter(!is.na(fp))
joint$FIXATED = (joint$fp > 0)



data = joint
dataN = data[data$fp > 0,]

# Centering
dataN = dataN %>% mutate(IsNamedEntity.Centered = (IsNamedEntity - mean(IsNamedEntity)))
dataN = dataN %>% mutate(LogWordFreq.Centered = (LogWordFreq - mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
dataN = dataN %>% mutate(HumanPosition.Centered = (HumanPosition - mean(HumanPosition))/sd(HumanPosition))
dataN = dataN %>% mutate(Condition.dummy.Centered = ifelse(Condition == "Preview", -0.5, 0.5))

# Centering surprisal & zeroing out OOV
dataN = dataN %>% mutate(Surprisal.Centered = (Surprisal - mean(ExperimentTokenLength, na.rm=TRUE))/sd(ExperimentTokenLength, na.rm=TRUE))
dataN[dataN$Surprisal_OOV_Status == "OOV",]$Surprisal.Centered = 0

# Centering word length
dataN = dataN %>% mutate(ExperimentTokenLength.Centered = (ExperimentTokenLength - mean(ExperimentTokenLength, na.rm=TRUE))/sd(ExperimentTokenLength, na.rm=TRUE))

# Centering answer status
dataN = dataN %>% mutate(IsCorrectAnswer.Centered = (IsCorrectAnswer - mean(IsCorrectAnswer, na.rm=TRUE)))



  
  # base model
  predictors = c("HumanPosition.Centered", "Condition.dummy.Centered", "LogWordFreq.Centered", "IsNamedEntity.Centered", "IsCorrectAnswer.Centered", "Surprisal.Centered", "ExperimentTokenLength.Centered")
  
  # for 
  current_components = predictors
  current_components = c(current_components, "(1|tokenID)", "(1|Participant)")
  
  TARGET_VARIABLE = "ff"
  
  
  #
    modelFP = lme4::lmer(formula(paste(TARGET_VARIABLE, " ~ ",paste(current_components, collapse=" + "), sep="")) , data=dataN, REML=FALSE)

  bestModelSoFarFP = modelFP
  
  foundNew = TRUE
  while(foundNew) {
    foundNew = FALSE
    bestNewLLFP = 1
    bestNewInteraction = NULL
    
    for(i in (1:length(predictors))) {
       for(j in (i:length(predictors))) {
         new_interaction = paste(predictors[i], predictors[j], sep="*")
         new_formula = paste(paste(current_components, collapse=" + "), new_interaction, sep=" + ")
         if(TARGET_VARIABLE != "FIXATED") {
             modelFPNew = lme4::lmer(formula(paste(TARGET_VARIABLE," ~ ",new_formula, sep="")) , data=dataN, REML=FALSE)
         } else {
            modelFPNew = lme4::glmer(formula(paste(TARGET_VARIABLE," ~ ",new_formula, sep="")) , data=data.Complete, family = binomial("logit"), REML=FALSE)
         }
  
         llFPNew = anova(modelFPNew, bestModelSoFarFP)$"Pr(>Chisq)"[2]
         if(llFPNew < bestNewLLFP) {
              bestNewInteraction = new_interaction
              bestNewLLFP = llFPNew
              bestModelInThisIterFP = modelFPNew
              cat(new_interaction, llFPNew, sep="\n")
              cat(new_formula, sep="\n")
          } else {
              cat(new_interaction, llFPNew, sep="\n")
          }
       } 
    }
         if(bestNewLLFP < 0.05) {
              foundNew = TRUE
              current_components = c(current_components, bestNewInteraction)
  
              bestModelSoFarFP = bestModelInThisIterFP
              cat("SELECTED")
              cat(TARGET_VARIABLE, "\t")
              cat(bestNewInteraction, bestNewLLFP, sep="\n")
              
              #cat(new_formula, sep="\n")
          } else {
              foundNew = FALSE
              #cat(new_interaction, llFP, llFPNew, sep="\n")
          }
  }
  
sink(paste("selection/selected_", TARGET_VARIABLE, ".txt", sep=""))
cat(paste(paste(current_components, collapse=" + "), new_interaction, sep=" + "))
sink()

