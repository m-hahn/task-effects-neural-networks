
\begin{table}$
\begin{center}$
\begin{tabular}${l c}$
\hline
 & Model 1 \\
\hline
(Intercept)                                          & 227.17 &  (6.84) &  $^{***}$ \\
PositionText                               & -1.10 &  (0.76) &          \\
Condition                             & 48.53 & (13.62) &  $^{***}$  \\
LogWordFreq                                 & -7.28 &  (0.28) &  $^{***}$  \\
IsNamedEntity                               & -10.14 &  (2.86) &  $^{***}$ \\
IsCorrectAnswer.Resid                                & -27.84 & (10.49) &  $^{**}$  \\
Surprisal.Resid                                      & 1.43 &  (0.30) &  $^{***}$   \\
WordLength.Resid                          & 8.11 &  (0.38) &  $^{***}$   \\
Condition:LogWordFreq        & -4.28 &  (0.39) &  $^{***}$  \\
LogWordFreq:IsNamedEntity          & -6.53 &  (0.76) &  $^{***}$  \\
Condition:WordLength.Resid & 4.11 &  (0.55) &  $^{***}$   \\
LogWordFreq:IsCorrectAnswer.Resid           & -12.69 &  (2.48) &  $^{***}$ \\
IsNamedEntity:Surprisal.Resid               & -1.96 &  (0.68) &  $^{**}$   \\
PositionText:IsCorrectAnswer.Resid         & -31.76 &  (8.89) &  $^{***}$ \\
PositionText:LogWordFreq          & 0.81 &  (0.25) &  $^{**}$    \\
Surprisal.Resid:WordLength.Resid          & 0.34 &  (0.13) &  $^{**}$    \\
IsCorrectAnswer.Resid:Surprisal.Resid                & -5.20 &  (1.98) &  $^{**}$   \\
\hline
AIC                                                  & 579667.34               \\
BIC                                                  & 579842.21               \\
Log Likelihood                                       & -289813.67              \\
Num. obs.                                            & 46334                   \\
Num. groups: tokenID                                 & 4950                    \\
Num. groups: Participant                             & 22                      \\
Var: tokenID (Intercept)                             & 1015.61                 \\
Var: Participant (Intercept)                         & 1003.69                 \\
Var: Residual                                        & 15070.74                \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001;  &  $^{**}$p<0.01;  &  $^{*}$p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
