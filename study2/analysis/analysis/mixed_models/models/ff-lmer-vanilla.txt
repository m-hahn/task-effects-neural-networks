
\begin{table}$
\begin{center}$
\begin{tabular}${l c}$
\hline
 & Model 1 \\
\hline
(Intercept)                                          & 201.60 & (4.71) &  $^{***}$ \\
PositionText                               & 0.74 & (0.45) &           \\
Condition                             & 20.09 & (9.40) &  $^{*}$    \\
LogWordFreq                                 & -6.30 & (0.52) &  $^{***}$  \\
IsNamedEntity                               & -2.73 & (1.42) &          \\
IsCorrectAnswer.Resid                                & -2.31 & (3.79) &          \\
Surprisal.Resid                                      & 1.27 & (0.19) &  $^{***}$   \\
WordLength.Resid                          & 0.40 & (0.24) &           \\
LogWordFreq:WordLength.Resid     & 1.06 & (0.23) &  $^{***}$   \\
Condition:WordLength.Resid & -0.89 & (0.35) &  $^{*}$    \\
LogWordFreq:Surprisal.Resid                 & 0.52 & (0.21) &  $^{*}$     \\
Surprisal.Resid:WordLength.Resid          & 0.17 & (0.08) &  $^{*}$     \\
Condition:IsCorrectAnswer.Resid       & -12.97 & (5.84) &  $^{*}$   \\
\hline
AIC                                                  & 537448.09              \\
BIC                                                  & 537587.99              \\
Log Likelihood                                       & -268708.05             \\
Num. obs.                                            & 46334                  \\
Num. groups: tokenID                                 & 4950                   \\
Num. groups: Participant                             & 22                     \\
Var: tokenID (Intercept)                             & 310.31                 \\
Var: Participant (Intercept)                         & 478.84                 \\
Var: Residual                                        & 6118.20                \\
\hline
\multicolumn{2}${l}${\scriptsize{ &  $^{***}$p<0.001;  &  $^{**}$p<0.01;  &  $^{*}$p<0.05}$}$
\end{tabular}$
\caption{Statistical models}$
\label{table:coefficients}$
\end{center}$
\end{table}$
