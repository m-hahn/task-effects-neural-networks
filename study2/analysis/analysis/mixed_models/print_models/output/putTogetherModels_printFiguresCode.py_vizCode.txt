IsCorrectAnswer:Surprisal, TotalTime
IsCorrectAnswer:WordLength, TotalTime
IsNamedEntity:Surprisal, TotalTime
IsNamedEntity:WordLength, TotalTime
LogWordFreq:IsCorrectAnswer, TotalTime
LogWordFreq:IsNamedEntity, TotalTime
PositionText:IsCorrectAnswer, TotalTime
PositionText:IsNamedEntity, TotalTime
Condition:IsCorrectAnswer, FirstFixation, TotalTime, FixationRate
Condition:IsNamedEntity, FixationRate
Condition:LogWordFreq, FirstPass, TotalTime, FixationRate
Condition:WordLength, FirstFixation, FirstPass, TotalTime
