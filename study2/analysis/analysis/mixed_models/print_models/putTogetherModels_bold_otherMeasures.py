
def printSig(beta, level):
    if "*" in level:
        return "\\textbf{"+str(beta)+"}"
    else:
        return str(beta)


def printHighlight(x,y):
    if ("Condition" in y or "CorrectAnswer" in y or "NamedEntity" in y) and "\\textbf" in x and ":" in y:
        return "\\textcolor{blue}{"+x+"}"
    else:
        return x

def formatLine(name, line):
    level = line[2]
    return [printHighlight(printSig(line[0], level), name), line[1]]

models = {x : open(f"models/{x}-lmer-vanilla.txt", "r").read().strip().split("\n") for x in ["rb", "rp", "sp"]}
models = {x : models[x][5:-16] for x in models}
models["FIXATED"] = open("models/FIXATED-lmer-BERNOULLI-brm.txt", "r").read().strip().split("\n")
#print(models)
#print(models["ff"])
predictors = set()
for x in models:
  if "FIXATED" in x:
     result = {}
     print("FIXATED", models[x])
     for line in models[x]:
        print(line)
        line= [x.strip() for x in line.split("\t")]
        predictor = line[0].replace("b_", "").replace(".Centered", "").replace("HumanPosition", "PositionText").replace(".dummy", "").replace("ExperimentTokenLength", "WordLength").strip().replace(".Resid", "")
        if predictor == "Intercept":
           predictor = "(Intercept)"
        predictors.add(predictor)
        level = float(line[3])
        level = min(level, (1-level))
        level = "$^{" + "*"*(0 if level > 0.05 else (1 if level > 0.01 else (2 if level > 0.005 else 3))) + "}$"
        result[predictor] = [str(y) for y in [printHighlight(printSig(round(float(line[1]),2), level), predictor), "("+str(round(float(line[2]),2))+")"]]
     models[x] = result
  else:
    assert models[x][0] == '\\hline'
    assert models[x][-1] == '\\hline'
    models[x]=models[x][1:-1]
    models[x] = [[z.strip() for z in y.replace("\\", "").split("&")] for y in models[x]]
    models[x] = {y[0].strip().replace(".Resid", "") : formatLine(y[0], y[1:]) for y in models[x]}
    for y in models[x]:
        predictors.add(y)
    print(models[x])
predictors_main = sorted([x for x in predictors if ":" not in x])
predictors_other = sorted([x for x in predictors if ":" in x and "Condition" not in x])
predictors_cond = sorted([x for x in predictors if ":" in x and "Condition" in x])






def flatten(y):
    r = []
    for x in y:
        for z in x:
            r.append(z)
    return r

def sameLengthForEachEntry(l):
    for i in range(len(l)):
        if len(l[i]) < 8:
            l[i] += " "*(8-len(l[i]))
    return l


def padEffect(x):
    return x+" "*(30-len(x))


print(predictors)
with open(f"output/{__file__}.tex", "w") as outFile:
  for group in [predictors_main, predictors_other, predictors_cond]:
    for effect in group:
        effects = [sameLengthForEachEntry(models[x].get(effect, ["", ""])) for x in ["rb", "rp", "sp"]]
        print(effects)
        if "blue" in effects[2][0] and group == predictors_other:
            for i in [0, 1, 2]:
                effects[i][0] = effects[i][0].replace("\\textcolor{blue}", "")
        print("&".join(flatten([[padEffect(effect.replace(".Resid", ""))]] + effects)) + " \\\\", file=outFile)
    print("\\hline", file=outFile)


