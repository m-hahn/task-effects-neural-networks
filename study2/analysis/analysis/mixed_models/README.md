# Mixed-Effects Analyses

* [Table 4 in main paper](print_models/output/putTogetherModels_bold.py.tex)

* [Table 1 in SI Appendix](print_models/output/putTogetherModels_brm_vanilla.py.tex)

* [Table 2 in SI Appendix](print_models/output/putTogetherModels_brm_log.py.tex)

* [Table 3 in SI Appendix](../mixed_models_noResid/print_models/output/putTogetherModels_bold.py.tex)


