
SAMPLING FOR MODEL '87a246627d579b9a8301fdc2f196f13f' NOW (CHAIN 1).
Chain 1: 
Chain 1: Gradient evaluation took 0.000774 seconds
Chain 1: 1000 transitions using 10 leapfrog steps per transition would take 7.74 seconds.
Chain 1: Adjust your expectations accordingly!
Chain 1: 
Chain 1: 
Chain 1: Iteration:    1 / 2000 [  0%]  (Warmup)
Chain 1: Iteration:  200 / 2000 [ 10%]  (Warmup)
Chain 1: Iteration:  400 / 2000 [ 20%]  (Warmup)
Chain 1: Iteration:  600 / 2000 [ 30%]  (Warmup)
Chain 1: Iteration:  800 / 2000 [ 40%]  (Warmup)
Chain 1: Iteration: 1000 / 2000 [ 50%]  (Warmup)
Chain 1: Iteration: 1001 / 2000 [ 50%]  (Sampling)
Chain 1: Iteration: 1200 / 2000 [ 60%]  (Sampling)
Chain 1: Iteration: 1400 / 2000 [ 70%]  (Sampling)
Chain 1: Iteration: 1600 / 2000 [ 80%]  (Sampling)
Chain 1: Iteration: 1800 / 2000 [ 90%]  (Sampling)
Chain 1: Iteration: 2000 / 2000 [100%]  (Sampling)
Chain 1: 
Chain 1:  Elapsed Time: 9.59821 seconds (Warm-up)
Chain 1:                9.29045 seconds (Sampling)
Chain 1:                18.8887 seconds (Total)
Chain 1: 

SAMPLING FOR MODEL '87a246627d579b9a8301fdc2f196f13f' NOW (CHAIN 2).
Chain 2: 
Chain 2: Gradient evaluation took 0.000285 seconds
Chain 2: 1000 transitions using 10 leapfrog steps per transition would take 2.85 seconds.
Chain 2: Adjust your expectations accordingly!
Chain 2: 
Chain 2: 
Chain 2: Iteration:    1 / 2000 [  0%]  (Warmup)
Chain 2: Iteration:  200 / 2000 [ 10%]  (Warmup)
Chain 2: Iteration:  400 / 2000 [ 20%]  (Warmup)
Chain 2: Iteration:  600 / 2000 [ 30%]  (Warmup)
Chain 2: Iteration:  800 / 2000 [ 40%]  (Warmup)
Chain 2: Iteration: 1000 / 2000 [ 50%]  (Warmup)
Chain 2: Iteration: 1001 / 2000 [ 50%]  (Sampling)
Chain 2: Iteration: 1200 / 2000 [ 60%]  (Sampling)
Chain 2: Iteration: 1400 / 2000 [ 70%]  (Sampling)
Chain 2: Iteration: 1600 / 2000 [ 80%]  (Sampling)
Chain 2: Iteration: 1800 / 2000 [ 90%]  (Sampling)
Chain 2: Iteration: 2000 / 2000 [100%]  (Sampling)
Chain 2: 
Chain 2:  Elapsed Time: 9.94236 seconds (Warm-up)
Chain 2:                12.7511 seconds (Sampling)
Chain 2:                22.6935 seconds (Total)
Chain 2: 

SAMPLING FOR MODEL '87a246627d579b9a8301fdc2f196f13f' NOW (CHAIN 3).
Chain 3: 
Chain 3: Gradient evaluation took 0.000228 seconds
Chain 3: 1000 transitions using 10 leapfrog steps per transition would take 2.28 seconds.
Chain 3: Adjust your expectations accordingly!
Chain 3: 
Chain 3: 
Chain 3: Iteration:    1 / 2000 [  0%]  (Warmup)
Chain 3: Iteration:  200 / 2000 [ 10%]  (Warmup)
Chain 3: Iteration:  400 / 2000 [ 20%]  (Warmup)
Chain 3: Iteration:  600 / 2000 [ 30%]  (Warmup)
Chain 3: Iteration:  800 / 2000 [ 40%]  (Warmup)
Chain 3: Iteration: 1000 / 2000 [ 50%]  (Warmup)
Chain 3: Iteration: 1001 / 2000 [ 50%]  (Sampling)
Chain 3: Iteration: 1200 / 2000 [ 60%]  (Sampling)
Chain 3: Iteration: 1400 / 2000 [ 70%]  (Sampling)
Chain 3: Iteration: 1600 / 2000 [ 80%]  (Sampling)
Chain 3: Iteration: 1800 / 2000 [ 90%]  (Sampling)
Chain 3: Iteration: 2000 / 2000 [100%]  (Sampling)
Chain 3: 
Chain 3:  Elapsed Time: 10.6735 seconds (Warm-up)
Chain 3:                9.48443 seconds (Sampling)
Chain 3:                20.1579 seconds (Total)
Chain 3: 

SAMPLING FOR MODEL '87a246627d579b9a8301fdc2f196f13f' NOW (CHAIN 4).
Chain 4: 
Chain 4: Gradient evaluation took 0.000176 seconds
Chain 4: 1000 transitions using 10 leapfrog steps per transition would take 1.76 seconds.
Chain 4: Adjust your expectations accordingly!
Chain 4: 
Chain 4: 
Chain 4: Iteration:    1 / 2000 [  0%]  (Warmup)
Chain 4: Iteration:  200 / 2000 [ 10%]  (Warmup)
Chain 4: Iteration:  400 / 2000 [ 20%]  (Warmup)
Chain 4: Iteration:  600 / 2000 [ 30%]  (Warmup)
Chain 4: Iteration:  800 / 2000 [ 40%]  (Warmup)
Chain 4: Iteration: 1000 / 2000 [ 50%]  (Warmup)
Chain 4: Iteration: 1001 / 2000 [ 50%]  (Sampling)
Chain 4: Iteration: 1200 / 2000 [ 60%]  (Sampling)
Chain 4: Iteration: 1400 / 2000 [ 70%]  (Sampling)
Chain 4: Iteration: 1600 / 2000 [ 80%]  (Sampling)
Chain 4: Iteration: 1800 / 2000 [ 90%]  (Sampling)
Chain 4: Iteration: 2000 / 2000 [100%]  (Sampling)
Chain 4: 
Chain 4:  Elapsed Time: 11.2358 seconds (Warm-up)
Chain 4:                12.0144 seconds (Sampling)
Chain 4:                23.2502 seconds (Total)
Chain 4: 
 Family: bernoulli 
  Links: mu = logit 
Formula: Correct ~ isPreview * tt + (1 + isPreview + tt + isPreview * tt | TextNo) + (1 + tt | Participant) 
   Data: humanScores (Number of observations: 418) 
  Draws: 4 chains, each with iter = 2000; warmup = 1000; thin = 1;
         total post-warmup draws = 4000

Group-Level Effects: 
~Participant (Number of levels: 22) 
                  Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
sd(Intercept)         0.80      0.37     0.18     1.62 1.00     1247     1519
sd(tt)                0.38      0.32     0.01     1.21 1.00     1786     2003
cor(Intercept,tt)     0.17      0.56    -0.91     0.96 1.00     3089     2621

~TextNo (Number of levels: 19) 
                            Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS
sd(Intercept)                   1.00      0.40     0.26     1.87 1.00     1090
sd(isPreview)                   1.09      0.65     0.09     2.54 1.00     1215
sd(tt)                          0.73      0.46     0.04     1.74 1.00      836
sd(isPreview:tt)                0.79      0.62     0.04     2.31 1.00     1666
cor(Intercept,isPreview)        0.20      0.40    -0.63     0.83 1.00     2303
cor(Intercept,tt)              -0.15      0.41    -0.85     0.66 1.00     2779
cor(isPreview,tt)              -0.07      0.42    -0.81     0.74 1.00     1926
cor(Intercept,isPreview:tt)    -0.03      0.44    -0.82     0.79 1.00     5022
cor(isPreview,isPreview:tt)     0.08      0.45    -0.79     0.83 1.00     3759
cor(tt,isPreview:tt)            0.10      0.44    -0.77     0.85 1.00     3188
                            Tail_ESS
sd(Intercept)                   1082
sd(isPreview)                   2124
sd(tt)                          1637
sd(isPreview:tt)                1801
cor(Intercept,isPreview)        2851
cor(Intercept,tt)               2697
cor(isPreview,tt)               3012
cor(Intercept,isPreview:tt)     2569
cor(isPreview,isPreview:tt)     3134
cor(tt,isPreview:tt)            3057

Population-Level Effects: 
             Estimate Est.Error l-95% CI u-95% CI Rhat Bulk_ESS Tail_ESS
Intercept        2.87      0.49     2.02     3.96 1.00     1720     2034
isPreview        2.67      0.82     1.24     4.42 1.00     1748     1982
tt               1.68      0.54     0.68     2.77 1.00     2074     2231
isPreview:tt     1.68      0.91     0.00     3.60 1.00     2175     1945

Draws were sampled using sampling(NUTS). For each parameter, Bulk_ESS
and Tail_ESS are effective sample size measures, and Rhat is the potential
scale reduction factor on split chains (at convergence, Rhat = 1).
