   humanScores = read.csv("../../experiment_data/eyetracking-data/scores.csv", header=TRUE,sep="\t")
   humanScores$isPreview = ((humanScores$Participant %% 2) == 0)
   humanScoresPreview = humanScores[humanScores$isPreview,]
   humanScoresNoPreview = humanScores[!humanScores$isPreview,]
   # exclude nonnative participants
   if(TRUE) {
   humanScoresNoPreview = humanScoresNoPreview[!(humanScoresNoPreview$Participant %in% c(5, 17)),]
   }

  humanScoresPreview$Participant = humanScoresPreview$Participant / 2
  humanScoresNoPreview$Participant = (humanScoresNoPreview$Participant + 1) / 2
  if(TRUE) {
    humanScoresPreview$Participant = paste("P", humanScoresPreview$Participant, sep="_")
    humanScoresNoPreview$Participant = paste("NP", humanScoresNoPreview$Participant, sep="_")
  }


  humanScores = rbind(humanScoresPreview, humanScoresNoPreview)
  humanScores$Participant = as.factor(humanScores$Participant)


library(tidyr)
library(dplyr)
library(lme4)
library(brms)

sink("output/correct.txt")
print(humanScores %>% group_by(isPreview) %>% summarise(Correct = mean(Correct)))
print(summary(glmer(Correct ~ isPreview + (1+isPreview|TextNo) + (1|Participant), data=humanScores, family="binomial")))
print(summary(brm(Correct ~ isPreview + (1+isPreview|TextNo) + (1|Participant), data=humanScores, family="bernoulli")))
sink()


data = read.csv("../../experiment_data/processed_data/aggregated_data_csv/data-full.csv")

times = data %>% filter(IsCorrectAnswer) %>% group_by(Participant, TextNo) %>% summarise(tt = sum(tt, na.rm=TRUE))


humanScores = merge(humanScores, times, by=c("Participant", "TextNo"), all=TRUE)


humanScores = humanScores %>% mutate(isPreview = isPreview-mean(isPreview, na.rm=TRUE))
humanScores = humanScores %>% mutate(tt = (tt-mean(tt, na.rm=TRUE))/sd(tt, na.rm=TRUE))

library(brms)

sink("output/correct_model.txt")
#print(summary(glmer(Correct ~ isPreview + tt + (1+isPreview+tt|TextNo) + (1+tt|Participant), data=humanScores, family="binomial")))
print(summary(brm(Correct ~ isPreview * tt + (1+isPreview+tt+isPreview*tt|TextNo) + (1+tt|Participant), data=humanScores, family="bernoulli")))
sink()




