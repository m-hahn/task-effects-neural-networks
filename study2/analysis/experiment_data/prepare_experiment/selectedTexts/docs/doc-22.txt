1051
   http://web.archive.org/web/20150723215530id_/http://www.dailymail.co.uk/news/article-3042930/Agony-family-daughter-28-living-controversial-church-cult-two-years-escapes-enticed-days-announces-getting-MARRIED-religious-elder.html

  /disk/scratch_ssd/deepmind/rc-data/dailymail/questions/test/2ad341d1058bc1474a291b97f233acde3dcf9fbd.question
  in a video posted to YouTube she announced plans to marry @placeholder , a church member , and denounce her family 
  Ronnie Saltsman 
.

Catherine Grove disappeared from Fayetteville, Arkansas, in July 2013
Two months later she was discovered living at the Church of Wells compound in Wells, Texas, saying she was 'seeking the Lord'
The church denies all forms of leisure in favor of prayer and believes in deep self-loathing to repent for sin
Grove left on April 2 and called 911, saying she needed assistance
She was with her parents for six days before returning to the church
In a video posted to YouTube she announced plans to marry Ronnie Saltsman, a church member, and denounce her family 
--
An Arkansas family who believe their daughter is being brainwashed by a controversial Texas church have again lost the woman, after she came home for a few days only to return to the 'religious cult'.
Then, within hours of returning to the Church of Wells - a small evangelical commune of about 70 'born of discontent with mainstream Christianity' - Catherine Grove, 28, announced she is set to marry one of its elders.
Grove initially vanished in July 2013, abandoning her car and belongings in Fayetteville, Arkansas, only to show up weeks later under heavy guard at the Church of Wells in Wells, Texas.
Over the years the church has been accused of sexual abuse, kidnapping and human trafficking, however they have strongly denied any wrongdoing.
Grove, a former nursing student, said she was 'seeking the Lord' and was not being held against her will, despite protestations from her parents.
At the time, her parents, Andy and Patty Grove, told reporters they believed their daughter had been brainwashed, amid mounting accusations over the years the Church of Wells is run as a cult, KNWA reported.
Then, earlier this month, Grove left the church under mysterious circumstances.
She called 911 on April 2 sounding disoriented, asking if someone could pick her up.
A sheriff was sent to get her, and Grove told police she left the church after having a fight with another member.
According to a report in the Lufkin Daily News, Grove stayed with her family for six days after that.
But she then returned to the Church of Wells.
Jake Gardner, one of three pastors that lead the church, released a statement to 5 News, lambasting the behavior of Grove's parents.
'May God open your eyes to behold the truth of what is really going on in this controversy,' Gardner said.
Gardner added: 'We are amazed at the atrocious behavior of the Groves towards their daughter, and pray they would be held accountable by those with a hand in this matter (as we have sought to do), to the end that their lives and behavior would agree with their confession of Christ.'
Grove then appeared herself in a YouTube video, explaining that her parents told her they visiting family in Little Rock but took her instead to a hospital.
'On April 7, I was still in the psych ward and the doctor came in and said after testing me all this time very carefully I had no mental illness so they cannot keep me in the hospital,' Grove said in the video.Grove said after this, church members would come and pick her up from the hospital.
'Here I am,' Grove said, smiling.
'(Now I'm) going to be married. Not sure the date. Even though my parents can be so difficult. 
'It is my desire to preach to them.'
Grove is set to marry Church of Wells member, Ronnie Saltsman. 
Grove insisted the reason she left the church was to find peace with her parents.
'I just wanted to remove this pressure upon me,' she said in the video.
'I wasn't trying to leave the church.'
Andy and Patty Grove are yet to comment publicly about their daughter returning to the church.
The Church of Wells denies members all forms of entertainment and leisure in favor of prayer.
They believe in deep self-loathing for their sin and that this is the only way to be saved.
Before Grove disappeared, the church hit headlines after a baby died at the compound.
An investigation following the incident in May 2012 showed the three-day-old infant died without receiving any medical attention.
Members of the church then carried the child around to different houses, where they prayed for the baby to be resurrected.
It would not be until 15 hours later that 911 was called, according to reports at the time.

