925
   http://web.archive.org/web/20150416171648id_/http://www.dailymail.co.uk/news/article-3031566/Tornadoes-hail-big-tennis-balls-winds-80mph-pepper-Midwest-experts-predict-57-million-people-risk-worse-come.html

  /disk/scratch_ssd/deepmind/rc-data/dailymail/questions/test/2a7ac6434c9ab5cf853ae5c0556fad1f5adbbcb9.question
  @placeholder estimates that 57 million people live in an area with an ' enhanced risk ' of hail , damaging winds and tornadoes 
  Storm Prediction Center 
.

Band of extreme weather predicted to hit an area including Chicago, Detroit and St. Louis, as well as Memphis, Tennessee, and Little Rock, Arkansas on Thursday
Storm Prediction Center estimates that 57 million people live in an area with an 'enhanced risk' of hail, damaging winds and tornadoes
Severe thunderstorms packing 80mph winds and large hail already made their way across central Missouri on Wednesday
Areas that don't see strong storms on Thursday could see heavy rain instead
--
The Midwest is bracing itself for worse to come after a monster storm impacted a 1,500-mile arc with 10 reported tornadoes, grapefruit-sized hail and winds of up to 80 mph on Wednesday.
The storm system, which stretches from Texas up to the Great Lakes and down to North Carolina, has the potential to be the biggest severe weather event so far this spring, according to The Weather Channel.
Hail of up to 4 inches in diameter smashed buildings and cars and high winds tore off roofs and downed trees, as wild weather hit 12 states. 
Strong storms rumbled through the Southern Plains early on Thursday, missing major population centers but offering a preview of bad weather that could hit Chicago, Detroit and other big cities in the Midwest later in the day.
The Storm Prediction Center said 57 million people lived in an area with an 'enhanced risk' of hail, damaging winds and tornadoes on Thursday. Tornadoes were reported on Wednesday and early Thursday in Kansas, Missouri and Oklahoma, but those areas saw minimal damage.
Meteorologists and emergency managers from the high Plains to the Appalachians were on alert as the U.S. had the year's first widespread bout of severe weather. The key message: Have a plan.
'Where to hide, emergency kits with medicines, snacks, water. Even something like sturdy shoes, gloves, long-sleeve shirts. If they get hit by a tornado they'll find they'll need those things pretty quickly,' said meteorologist Erin Maxwell with the National Weather Service in Norman, Oklahoma.
'Know what you're doing, and just don't panic,' Maxwell added.
Severe thunderstorms packing 80mph winds and large hail made their way across central Missouri on Wednesday afternoon, including several capable of producing tornadoes.
Weather spotters reported a funnel cloud near Potosi in eastern Missouri at 3:35 p.m., while an hour earlier the Bates County emergency manager reported a tornado in southwest Missouri that destroyed a 60-foot machine shop.
Indiana State Police said high winds toppled a tractor-trailer on Interstate 69 near Evansville, while utilities reported a number of power outages after wind gusts reached 70 mph.
Fewer than 1 million people were covered by Wednesday's 'moderate risk' area between Wichita, Kansas, and Jefferson City, Missouri. Thursday's worst weather was predicted in an area that included Chicago, Detroit and St. Louis, as well as those in Memphis, Tennessee, and Little Rock, Arkansas.
Areas that won't see strong storms on Thursday could see heavy rain instead.

