import random
import sys

buttonMap = ["X", "Y", "A", "B"]

SETUP = sys.argv[1] # preview or nonpreview

def breakIntoLines(text):
  lines = []
  words = text.split(" ")
  currentLineLength = 0
  currentLine = ""
  for word in words:
      if len(word) + currentLineLength + 1 <= 85:
         currentLine += word+" "
         currentLineLength += len(word) + 1
      else:
         lines.append(currentLine.rstrip())
         currentLine = word+" "
         currentLineLength = len(word)
  lines.append(currentLine.rstrip())
  return lines

def breakIntoPages(lines):
   pages = []
   for i in range(0, len(lines), 11):
      pages.append("\\n".join(lines[i:i+11]))
      if len(lines) == i+11+1:
        pages[-1] = pages[-1]+"\\n"+lines[i+11]
        break
   
   return pages



print("""%BeginHeader

%EndHeader

set conditions =  1
set experiments = 1
set expConditions = 0
set background =  16777215
set foreground =  0
set filterMode = 2
set windowThreshold = 0
set calibration_type = 1
set calibration_height = 51475000
set display_type = LCD

trial_type Question
  text_format =    'Lucida Sans Typewriter' 20 20 113 263 cleartype
  text_weight =    normal non-italic
  button =         Y
  button =         X
  button =         B
  button =         A
  output =         nostream
  trigger =        nogaze
  cursor_size =    0
  dc_delay =       0
  stimulus_delay = 0
  revert =         0
  highlight_color =983988
end Question
""")

trigger = "driftcorrect"

if SETUP == "preview":
  trigger = "nogaze"
  print("""trial_type QuestionPreview
  text_format =    'Lucida Sans Typewriter' 20 20 113 263 cleartype
  text_weight =    normal non-italic
  button =         leftTrigger
  button =         rightTrigger
  output =         nostream
  trigger =        driftcorrect
  cursor_size =    0
  dc_delay =       0
  stimulus_delay = 0
  revert =         0
  highlight_color =983988
end QuestionPreview
""")

print("""trial_type Text
  text_format =    'Lucida Sans Typewriter' 20 20 113 113 cleartype
  text_weight =    normal non-italic
  button =         leftTrigger
  button =         rightTrigger
  output =         nostream
  trigger =        nogaze
  cursor_size =    10
  dc_delay =       0
  stimulus_delay = 0
  revert =         0
  highlight_color =983988
end Text

trial_type TextTrigger
  text_format =    'Lucida Sans Typewriter' 20 20 113 113 cleartype
  text_weight =    normal non-italic
  button =         leftTrigger
  button =         rightTrigger
  output =         nostream
  trigger =        """+trigger+"""
  cursor_size =    0
  dc_delay =       0
  stimulus_delay = 0
  revert =         0
  highlight_color =983988
end TextTrigger""")

itemCounter = 0

trainingNumber = 2


listFile = open("selection.txt", "r")
numberOfDependentPages = {}
correctAnswers = {}
originalFiles = {}
EP = "P"
for fileName in listFile:
   if fileName[0] == "#":
      continue
   itemCounter += 1
   if itemCounter == trainingNumber+1:
      EP = "E"
   fileName = fileName.rstrip()
   originalFiles[itemCounter] = fileName
   itemFile = open("CORPUS/"+fileName, "r")
   mode = 0
   text = ""
   question = ""
   correctAnswer = ""
   answers = []
   for line in itemFile:
      if len(answers) == 4:
         break
      line = line.rstrip().replace("  "," ")
      if len(line) == 0:
         continue
      elif line == "--":
         mode += 1
      elif mode == 1:
         text = text+" "+line
      elif mode == 2:
         if line[0] == "+":
            correctAnswers[itemCounter] = line[1:].lstrip()
            answers.append(line[1:].lstrip())
         elif line[0] == "-":
            answers.append(line[1:].lstrip())
         elif line[0:2] != "0.":
            question = line.lstrip()
   question = question.replace("@placeholder", "__________")




   if len(answers) < 4:
     print("ERROR "+fileName)
     continue

#   print("--------")
#   print(fileName)
#   print re.sub('[ -~]', '', text)+"|"
#   print re.sub('[ -~]', '', answers[0])+"|"
#   print re.sub('[ -~]', '', answers[1])+"|"
#   print re.sub('[ -~]', '', answers[2])+"|"
#   print re.sub('[ -~]', '', answers[3])+"|"
#   print re.sub('[ -~]', '', question)+"|"




   random.shuffle(answers)
   correctAnswerButton = buttonMap[answers.index(correctAnswers[itemCounter])]


   dependentCounter = 0
   itemNumber = itemCounter
   if EP == "E":
     itemNumber -= trainingNumber
   if SETUP == "preview":
     print("")
     print("trial "+EP+"1I"+str(itemNumber)+"0"+str(dependentCounter)+"D"+str(dependentCounter))
     print("  gc_rect =          (0 0 0 0)")
     print("  inline =           |"+"\\n".join(breakIntoLines(question)))
     print("  max_display_time = 300000")
     print("  trial_type =       QuestionPreview")
     print("end "+EP+"1I"+str(itemNumber)+"0"+str(dependentCounter)+"D"+str(dependentCounter))
     dependentCounter += 1 

   # the text
   textLines = breakIntoLines(text.lstrip())
   pages = breakIntoPages(textLines)
   pageCounter = 0
   for page in pages:
         pageCounter += 1
         print("")
         print("trial "+EP+"1I"+str(itemNumber)+"0"+str(dependentCounter)+"D"+str(dependentCounter))
         print("  gc_rect =          (0 0 0 0)")
         print("  inline =           |"+page)
         print("  max_display_time = 300000")
         if pageCounter == 1:
             type = "TextTrigger"
         else:
             type = "Text"
         print("  trial_type =       "+type)
         print("end "+EP+"1I"+str(itemNumber)+"0"+str(dependentCounter)+"D"+str(dependentCounter))

         dependentCounter += 1
   # the question
   print("")
   print("trial "+EP+"1I"+str(itemNumber)+"0"+str(dependentCounter)+"D"+str(dependentCounter))
   print("  button =           "+correctAnswerButton)
   print("  gc_rect =          (0 0 0 0)")
   print("  inline =           |"+"\\n".join(breakIntoLines(question))+"\\n\\n"+"(1) "+answers[0]+" (2) "+answers[1]+" (3) "+answers[2]+" (4) "+answers[3])
   print("  max_display_time = 300000")
   print("  trial_type =       Question")
   print("end "+EP+"1I"+str(itemNumber)+"0"+str(dependentCounter)+"D"+str(dependentCounter))
   numberOfDependentPages[itemCounter] = dependentCounter

for item in numberOfDependentPages:
   if item <= trainingNumber:
      EP = "P"
   else:
      EP = "E"
   print("")
   itemNumber = item
   if item > trainingNumber:
      itemNumber = item - trainingNumber
   print("sequence S"+EP+"1I"+str(itemNumber))
   for i in range(0, numberOfDependentPages[item]+1):
     print("  "+EP+"1I"+str(itemNumber)+"0"+str(i)+"D"+str(i))
   print("end S"+EP+"1I"+str(itemNumber))


#print(numberOfDependentPages)
#print(correctAnswers)
#print(originalFiles)


#print((sum(numberOfDependentPages.itervalues())+0.0) / len(numberOfDependentPages))








