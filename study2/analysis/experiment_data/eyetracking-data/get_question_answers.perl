#!/usr/bin/perl -w

# note that for us in the ASC file 1 means "yes" and 2 means "no".

# output: a number of lines, each of which contains three fields:
# 1) item ID
# 2) correct answer (1=yes,0=no)
# 3) participant's response (1=yes,0=no)

$fileName = shift;

#print($fileName);

open($fh, $fileName);


$fileName =~ /\/S0*(\d*)_/;

$subject_number = $1;

@results = ();


while(<$fh>) {
  chomp;
  chomp;
  if(/TRIALID ([^ ]+D[1-9])/) {
    $id = $1;
  }
  elsif(/QUESTION_ANSWER ([1234])/) {
    $correct = $1;
  }
  elsif(/^BUTTON\s+[0-9]+\s+([1234])\s+1/) {
    $response = $1;
    if($id && $correct) {
#      print "$id\t", $correct,"\t", $response, "\n"; # always comes last
#      $total++;
      $correct_difference = 0;
      if ($response==$correct) {
        $correct_difference = 1;
      }
 #     $all_correct += $correct_difference;
#      print($id =~ /E1/);
      if ($id =~ /E1I(\d*)0\dD/) {
        #print $1," ",$id," ",$correct_difference,"\n";
        $results[int($1)-1] = $correct_difference;
#        print(@results);
      }
      $id = ""; $correct = "";
    }
  }
}
#print("\n ");

for($i=0;$i<@results;$i++) { 
  print $subject_number,"\t",($i+1), "\t",$results[$i],"\n";
}
#print("\n");
#print STDERR "Correct: $all_correct/$total (" , ($all_correct/$total), "\%)\n";

# sub recode {
#   return ($_[0]==1 ? 1 : 0);
# }
