import re

def fileName(number):
  if number % 2 == 0:
    setup = "PRE"
  else:
    setup = "NPR"
  if number < 10:
    number = "0"+str(number)
  return "/disk/scratch/deepmind-experiment-scratch/S"+str(number)+"_"+setup+".asc.txt"


def numberFormatting(number):
  return str(round(number,1))

initialInNullLine = []
finalInNullLine = []


for fileNum in range(1,25):
        nameOfInFile = fileName(fileNum)
        print("PROCESS FILE "+nameOfInFile)
	inFile = open(nameOfInFile, "r")


	linesToObjects = {}

	splitterSingle = re.compile("[ |\t]")
	splitterMultiple = re.compile("[ |\t]*")


	regions = []
	fixDataPoints = []
	sacDataPoints = []

	trialCounter = -1
	counter=0
	fixationCounter = 0
        hadACalibration = []
	for line in inFile:
	    counter+=1
	    line = line.rstrip()
	    if line[0:2] == "**":
	      hullu = 1
            elif line.startswith(">>>>>>> CALIBRATION"):
              if len(hadACalibration) > 0:
                 hadACalibration[len(hadACalibration)-1] = 1
	    elif line[0:3] == "MSG":
	       data = splitterSingle.split(line[4:])
	       if data[1] == "!CAL":
		   hullu = 1
	       elif data[1] == "Recorded":
		  hullu = 1
	       elif data[1] == "TRIALID":
		 fixationCounter=0
		 trialCounter+=1
		 regions.append([])
		 fixDataPoints.append([])
		 sacDataPoints.append([])
                 hadACalibration.append(0)
	       elif data[1] == "DISPLAY":
		    hullu = 1
	       elif data[1] == "REGION": #902316 REGION CHAR 9 1 _ 295 351 311 408
	#           print(data)
		   if data[5] == '':
		      del data[5]
		   x1 = int(data[6])
		   y1 = int(data[7])
		   x2 = int(data[8])
		   y2 = int(data[9])
		   regions[trialCounter].append([x1,y1,x2,y2])
	       elif data[1] == "DELAY":
		 hullu = 1
	    elif line[0:4] == "SFIX":
	      hullu = 1
	    elif line[0:4] == "EFIX":
	#R 905378	905542	165	  630.7	  525.6	    494
	      fixationCounter+=1
	      if fixationCounter > 1:
		data = splitterMultiple.split(line[5:])
		x = float(data[4])
		y = float(data[5])
		fixDataPoints[trialCounter].append([x,y])
		linesToObjects[counter] = [trialCounter,len(fixDataPoints[trialCounter])-1]
	   #   print(x+" "+y)
	    elif line[0:5] == "SSACC":
	      hullu = 1
	    elif line[0:5] == "ESACC":
	      #print(line)
	      #ESACC R  858392	858419	28	  143.1	  240.1	  375.5	  237.4	   5.94	    448
	      data = splitterMultiple.split(line[6:])
	      #print(data)
	      startX = (data[4])
	      startY = (data[5])
	      endX = (data[6])
	      endY = (data[7])
	      sacDataPoints[trialCounter].append([startX,startY,endX,endY])
	      linesToObjects[counter] = [trialCounter,len(sacDataPoints[trialCounter])-1]
	  #    print(startX+" "+startY+" "+endX+" "+endY)

	#print(fixDataPoints)
	inFile.close()



	lineStartsX = 0
	linesHeight = 57
	regionWidth = 16
	linesStartY = [151, 218, 285, 352, 419, 486, 553, 620, 687, 754, 821, 888]

        if len(hadACalibration) > 0:
          hadACalibration[0] = 1
#        print(hadACalibration)
 #       crash()

	def findLineOfFixation(trial, coordinates):
	#   print(">>")
	 #  print(coordinates)
	   for i in range(0,12):
	      if coordinates[1] < linesStartY[i]:
		return -1
	      elif coordinates[1] < linesStartY[i] + linesHeight:
		return i
	   return -1

	def findLine(y):
	   if y in linesStartY:
	     return linesStartY.index(y)
	   elif y+1 in linesStartY:
	     return linesStartY.index(y+1)
	   elif y+2 in linesStartY:
	     return linesStartY.index(y+2)
	   else:
	     print(y)
	     crash()

	numberOfRegionsPerLinePerTrial = []
	for i in range(0, len(regions)): #iterate over the trials
	   numberOfRegionsPerLine = [0,0,0,0,0,0,0,0,0,0,0,0]
	   
	   for region in regions[i]:
	     line = findLine(region[1])
	     numberOfRegionsPerLine[line] += 1

	   numberOfRegionsPerLinePerTrial.append(numberOfRegionsPerLine)



	correctionPre = [
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0,
		     0]


	positionsPre = [
		     [0,0], # (1,1) # 1
		     [800,0], #(2,1) #2
		     [1600,0], # (3,1) # 3
		     [0,500], # (1,2) #4
		     [800,500], #(2,2) #5
		     [1600,500], # (3,2) #6
		     [0,1000], # (1,3) #7
		     [800,1000], # (2,3) #8
		     [1600,1000] # (3,3) #9
		    ]

	corrections = []
	for i in range(0,len(fixDataPoints)):
	    corrections.append(list(correctionPre))

	def getCoefficients(x, p1,p2,p3):
	   if x < p2:
	      coef1X = max((0.0 + p2 - x) / p2, 0.0)
	      coef2X = 1 - coef1X
	      coef3X = 0.0
	   else:
	      coef3X = max((0.0+x-p2) / p2, 0.0)
	      coef2X = 1 - coef3X
	      coef1X = 0.0
	   return [coef1X,coef2X,coef3X]


	def calculateInfluence(trial, x, y, c1, c2): # (c1,c2) which point it is (both from 1 to 3)
	   coefX = getCoefficients(x,0,800,1600)
	   coefY = getCoefficients(y,0,500,1000)
	   return (coefX[c1-1] * coefY[c2-1])

	def calculateCoefficients(trial, x, y):
	   coef = [
		   calculateInfluence(trial,x, y, 1, 1),
		   calculateInfluence(trial,x, y, 1, 2),
		   calculateInfluence(trial,x, y, 1, 3),
		   calculateInfluence(trial,x, y, 2, 1),
		   calculateInfluence(trial,x, y, 2, 2),
		   calculateInfluence(trial,x, y, 2, 3),
		   calculateInfluence(trial,x, y, 3, 1),
		   calculateInfluence(trial,x, y, 3, 2),
		   calculateInfluence(trial,x, y, 3, 3)]
	   total = sum(coef)
	   #print(total)
	   for i in range(0,9):
	      coef[i] = coef[i] / total
	   return coef

	positions =  [
		      [1,1],
		      [1,2],
		      [1,3],
		      [2,1],
		      [2,2],
		      [2,3],
		      [3,1],
		      [3,2],
		      [3,3],
		     ]


	def calculateInfluenceFromNum(trial, x, y, num):
	   return calculateInfluence(trial, x, y, positions[num][0], positions[num][1])
	   


	import copy
	correctedFixDataPoints = copy.deepcopy(fixDataPoints)

	def selectVariable(var, counterAtMax):
	   if var[1] < 8:
	      return [var[0], var[1]+1]
	   elif var[0] < len(corrections)-1:
	     if counterAtMax:
	       return [var[0]+1, 0]
	     else:
	       return [var[0],0]
	   else:
	      return -1



	import math
	import numpy

	def implementCorrection(variable, correctionDifference):
	   for i in range(0,len(correctedFixDataPoints[variable[0]])):
	      influence = calculateInfluenceFromNum(variable[0], fixDataPoints[variable[0]][i][0], fixDataPoints[variable[0]][i][1], variable[1])
	      correctedFixDataPoints[variable[0]][i][1] = correctedFixDataPoints[variable[0]][i][1] + correctionDifference * influence

	varianceOffCenter = -1
	varianceInNullLine = -1
	varianceOutOfRegion = -1

	correctionVariance = -1



	##
	maxCounter = -1
	STEP_SIZE = -1
	RANGE = -1
	inARepetition = 0

	def resetParameters():
	  global maxCounter
	  global STEP_SIZE
	  global RANGE
	  global inARepetition
	  maxCounter = 4
	  STEP_SIZE = 3
	  RANGE = 60
	  inARepetition = 0

	  global varianceOffCenter
	  global varianceInNullLine
	  global varianceOutOfRegion
	  global correctionVariance
	  varianceOffCenter = 100
	  varianceInNullLine = 0.45
	  varianceOutOfRegion = 5000

	  correctionVariance = 150




	resetParameters()
	##




	def calculateOverallValue(variable, correctionDifference):
	   value = 0
	   # deviance from previous one
	#   if variable[0] > 0:
	#      value *= math.exp(- abs(corrections[variable[0]-1][variable[1]] - corrections[variable[0]][variable[1]] - correctionDifference) ** 2 / 10)
	   value += - abs(corrections[variable[0]][variable[1]] + correctionDifference) ** 2 / (correctionVariance)
	   perLine = [0,0,0,0,0,0,0,0,0,0,0,0]
	   inNullLine = 0
	   previousLine = -1
	   lineSwitches = 0
	   outOfRegion = 0
	   offFromCenter = 0
           numberOfFixationsOutOfRegion = 0
	   for i in range(0,len(correctedFixDataPoints[variable[0]])):
	      influence = calculateInfluenceFromNum(variable[0], fixDataPoints[variable[0]][i][0], fixDataPoints[variable[0]][i][1], variable[1])
	      newCorrected =  correctedFixDataPoints[variable[0]][i][0:2]
	      newCorrected[1] = newCorrected[1] + correctionDifference * influence

	      line = findLineOfFixation(variable[0], newCorrected)
	      
	      if newCorrected[1] < 151:
		 outOfRegion += (newCorrected[1] - 151)**2
                 numberOfFixationsOutOfRegion += 1
	      if newCorrected[1] > 945:
		 outOfRegion += (newCorrected[1] - 945)**2 
                 numberOfFixationsOutOfRegion += 1
	      if line != -1:
		if numberOfRegionsPerLinePerTrial[variable[0]][line] * regionWidth + 151 < newCorrected[1]:
		  outOfRegion += ((numberOfRegionsPerLinePerTrial[variable[0]][line] * regionWidth + 151) - newCorrected[1])**2
                  numberOfFixationsOutOfRegion += 1
		else:
		  offFromCenter += (newCorrected[1] - linesStartY[line] - linesHeight/2)**2
	      if line == -1:
		inNullLine += 1
	      else:
		perLine[line] = perLine[line]+1
		if line != previousLine:
		  lineSwitches += 1
		  previousLine = line


	#   mean = numpy.mean(perLine[0:numberOfLinesPerTrial[variable[0]]])
	#   for i in range(0, numberOfLinesPerTrial[variable[0]]):
	 #      value *= math.exp(- (perLine[i] - mean)**2 / 10)
	#   print(offFromCenter)

	   value += (- offFromCenter / (2*varianceOffCenter))
	   value += (- inNullLine**2 /(2*varianceInNullLine))
	   value += (- lineSwitches**2/1)
	   value += (- outOfRegion/(2*varianceOutOfRegion))
	   return [value, offFromCenter, inNullLine, outOfRegion, numberOfFixationsOutOfRegion]

	#   return [value, offFromCenter, inNullLine, outOfRegion,  abs(corrections[variable[0]][variable[1]] + correctionDifference) ** 2 / (150),  outOfRegion/10000 , lineSwitches]


	variable = [0,0]


	import matplotlib.pyplot as plt
	import numpy as np




	def makePlot(trial):
	  fig = plt.figure()
	  ax1 = fig.add_subplot(121)
	  ax2 = fig.add_subplot(122)


	## the data
	  N=1000
	  x = [0,1600]
	  xC = [0,1600]
	  y = [0,1000]
	  yC = [0,1000]
	  for coord in fixDataPoints[trial]:
	     x.append(coord[0])
	     y.append(coord[1])
	  for coord in correctedFixDataPoints[trial]:
	     xC.append(coord[0])
	     yC.append(coord[1])

	  for node in range(0,9):
	     x.append(positionsPre[node][0])
	     xC.append(positionsPre[node][0])
	     y.append(positionsPre[node][1])
	     yC.append(positionsPre[node][1] + corrections[trial][node])



	## left panel
	  ax1.scatter(x,y,color='blue',s=5,edgecolor='none')
	  ax1.set_aspect(1./ax1.get_data_ratio()) # make axes square

	  ax2.scatter(xC,yC,color='blue',s=5,edgecolor='none')
	  ax2.set_aspect(1./ax2.get_data_ratio()) # make axes square
	  for i in range(0,12):
	     ax1.plot([0,1600], [linesStartY[i],linesStartY[i]], 'k-', lw=1)
	     ax1.plot([0,1600], [linesStartY[i]+linesHeight,linesStartY[i]+linesHeight], 'k-', lw=1)
	     ax2.plot([0,1600], [linesStartY[i],linesStartY[i]], 'k-', lw=1)
	     ax2.plot([0,1600], [linesStartY[i]+linesHeight,linesStartY[i]+linesHeight], 'k-', lw=1)

	  plt.show()








	#   value += (- offFromCenter / 200)
	#   value += (- inNullLine**2 /0.9)
	   #value += (- lineSwitches**2/1)
	#   value += (- outOfRegion/10000)


	#offFromCenter, inNullLine, outOfRegion,
	if 0:
	 variable = [0,0]
	 variances= [0,0,0]
	 numberOfItems = 0.0
	 # induce parameters: get mean and variance of the values
	 while 1:
	  variable = selectVariable(variable, 1)
	  if variable == -1:
	    break

	  valueList = calculateOverallValue(variable, 0)

	  #offCenter = valueList[1]
	 # inNullLine = valueList[2]
	#  outOfRegion = valueList[3]
	  for r in range(0,3):
	     variances[r] += pow(valueList[r+1]  ,2)
	  numberOfItems += 1

	  print(valueList[1:4])
	#variances = [0,0,0]
	 for r in range(0,3):
	  variances[r] /= numberOfItems
	  print(math.sqrt(variances[r]))
	 print(variances)
	#varianceOffCenter = variances[0]
	#varianceInNullLine = variances[1]
	#varianceOutOfRegion = variances[2]
	#crash()


	variable = [0,0]


	sumValuesPrevious = [0,0,0,0,0]
	sumValuesPost = [0,0,0,0,0]

	##

	counter = 0
	wasAtNonzeroDifference = -1
	while 1:
	 if counter == 0:
	   if variable[1] == 0:
	     wasAtNonzeroDifference = 0
	 if variable[1] == 0 and counter == 0:
	    valueList = calculateOverallValue(variable, 0)
	    print("Trial "+str(variable[0]))
	    print("  INITIAL\t"+str(valueList[2]+0.0)+"\t"+str(valueList[4]+0.0))#+"\t"+str((valueList[2]+0.0)/(len(fixDataPoints[variable[0]])+0.0001) ))
            initialInNullLine.append(valueList[2]+0.0)
	    sumValuesPrevious = [x+y for x,y in zip(sumValuesPrevious, valueList)]
	 variable = selectVariable(variable, counter == 0)
	 if variable == -1:
	   break
	 if (variable[1] == 8):
	   counter += 1
	 bestValue = 2
	 bestValueList = []
	 bestCorrectionDifference = 0
	 if not (variable == -1):
	   correctionSoFar = corrections[variable[0]][variable[1]]
	   for correctionDifference in range(-RANGE-correctionSoFar,RANGE-correctionSoFar, STEP_SIZE):
	       valueList = calculateOverallValue(variable, correctionDifference)
	       value = valueList[0]
	       if bestValue > 1 or value > bestValue:
		  bestValue = value
		  bestValueList = valueList
		  bestCorrectionDifference = correctionDifference
	 #  print(str(variable)+"  "+str(counter))
	#   print(bestValueList)
	#   if counter == maxCounter:

	   implementCorrection(variable, bestCorrectionDifference)
	   corrections[variable[0]][variable[1]] = corrections[variable[0]][variable[1]] + bestCorrectionDifference 

	   if 0 and counter == maxCounter:
	    lastLine = -1
	    for i in range(0, len(correctedFixDataPoints[variable[0]])):
	      newCorrected = correctedFixDataPoints[variable[0]][i]
	      line = findLineOfFixation(variable[0], newCorrected)
	      if line == -1:
	       if newCorrected[1] < 151:
		  correctedFixDataPoints[variable[0]][i][1] = 152
	       elif newCorrected[1] > 945:
		  correctedFixDataPoints[variable[0]][i][1] = 944
	       else:
		 if lastLine != -1:
		   if newCorrected[1] > linesStartY[lastLine]:
		     newCorrected[1] = linesStartY[lastLine] + linesHeight - 1
		   else:
		     newCorrected[1] = linesStartY[lastLine] + 1
	      else:
	       lastLine = line
		  

		   


	   



	 
	   if (0 or ((variable[1] == 8) and counter == maxCounter)):
	      print("  FINAL\t"+str(bestValueList[2]+0.0)+"\t"+str(bestValueList[4]+0.0))#+"\t"+str((bestValueList[2]+0.0)/(len(fixDataPoints[variable[0]])+0.00001) ))
	      print("  CORR\t"+str(corrections[variable[0]]))
	      print("---")
	      sumValuesPost = [x+y for x,y in zip(sumValuesPost, bestValueList)]
	      if bestValueList[2] > 10:
		if not inARepetition:
		  print("Repeating")
		  inARepetition = 1
		  counter = 1
		  RANGE = 120
		  STEP_SIZE = 1
		  correctionVariance = 500
		#else:
		  #makePlot(variable[0])
	   if variable[1] == 8 and counter == maxCounter:
              finalInNullLine.append(bestValueList[2]+0.0)


              #if inARepetition:
	        #makePlot(variable[0])

              # at the end, assuming no repetition was started or the program has already finished the repetition
              #makePlot(variable[0])
	      counter = 0
	      resetParameters()
	 else:
	   break

	sumValuesPost = [x/(0.0+len(fixDataPoints)) for x in sumValuesPost]
	sumValuesPrevious = [x/(0.0+len(fixDataPoints)) for x in sumValuesPrevious]

	print("EVALUATION: (value, off from center, in null line, out of bounds)")
	print(sumValuesPrevious)
	print(sumValuesPost)


        def computeCorrected(x, y, trial):
           correctedY = y
           for variable in range(0,9):
             influence = calculateInfluenceFromNum(trial, x, y, variable)
             correctedY += corrections[trial][variable] * influence
           return correctedY

        def computeCorrectedFromString(x, y, trial):
           try:
              x= float(x)
              y = float(y)
              return numberFormatting(computeCorrected(x,y,trial))
           except ValueError:
              return y


        nameOfInFile = fileName(fileNum)
        nameOfOutFile = nameOfInFile+".RESU.asc"

	inFile = open(nameOfInFile, "r")

	outFile = open(nameOfOutFile, "w")

	counter = 0
	for line in inFile:
	    counter+=1
	    line = line.rstrip()
	    if counter in linesToObjects:
               if line[0:4] == "EFIX":
                 data = splitterMultiple.split(line[5:])
                 dataIndices = linesToObjects[counter]
                 trial = dataIndices[0]
                 dataPoint = dataIndices[1]
                 data[4] =  numberFormatting(correctedFixDataPoints[trial][dataPoint][0])
                 data[5] =  numberFormatting(correctedFixDataPoints[trial][dataPoint][1])
# todo format to 168.4 format
                 outLine = "EFIX R   "+data[1]+"\t"+data[2]+"\t"+data[3]+"\t  "+data[4]+"\t  "+data[5]+"\t    "+data[6]
                 #print(outLine) 
                 #print(line)
                 outFile.write(outLine+"\r\n")
               elif line[0:5] == "ESACC":
                 dataIndices = linesToObjects[counter]
                 trial = dataIndices[0]
                 dataPoint = dataIndices[1]

                 data = splitterMultiple.split(line[6:])
                 #print([data[4], data[5]])
                 data[5] = computeCorrectedFromString(data[4], data[5], trial)
                 data[7] = computeCorrectedFromString(data[6], data[7], trial)
                 outLine = "ESACC R  "+data[1]+"\t"+data[2]+"\t"+data[3]+"\t "+data[4]+"\t  "+data[5]+"\t "+data[6]+"\t  "+data[7]+"\t   "+data[8]+"\t    "+data[9]
           #      print("NEW "+outLine) 
            #     print("OLD "+line)
                 outFile.write(outLine+"\r\n")
	    else:
	       outFile.write(line+"\r\n")

	outFile.close()
	inFile.close()


print("--")
if len(initialInNullLine) != len(finalInNullLine) :
  print("WARNING")
  print(len(initialInNullLine))
  print(len(finalInNullLine))
  print("--")

for i in range(0, len(initialInNullLine)):
   print(str(initialInNullLine[i])+"\t"+str(finalInNullLine[i]))


