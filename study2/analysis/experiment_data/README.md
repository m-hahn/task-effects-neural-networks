
Prepare experiment
* `prepare_experiment/selectedTexts/CORPUS/*.txt`
* `prepare_experiment/prepareExperiment.py`
* `prepare_experiment/selection.txt`

Experiment:
* `eyetracking-data/subject_notes`

Postprocessing:
* `get_reading_measures/eyetrack-postprocess/postprocess.py` applies automatic correction to eyetracking data

Experiment results:
* `eyetracking-data/scores.csv`
* `eyetracking-data/score.sh`
* `eyetracking-data/get_question_answers.perl`
* `human-results/nopreview.fp`
* `human-results/nopreview.tt`
* `human-results/preview.fp`
* `human-results/preview.tt`


* `prepare_experiment/selectedTexts/prepare_text.py`
* `processed_data/forEvaluation/createMappingForSampleFiles.py`
* `processed_data/forEvaluation/mergeSurprisals.py`
* `processed_data/forEvaluation/prepareItemsForEvaluation.py`
* `processed_data/forEvaluation/mappingWithHuman/collectMappingWithHumanFiles.py`
* `processed_data/forEvaluation/questions/extractCoefficients.py`
* `processed_data/forEvaluation/questions/prepareFileListForR.py`
* `processed_data/forEvaluation/questions/putExternalMeasuresIntoMappingTable.py`
* `processed_data/forEvaluation/questions/putReadingMeasuresIntoMappingTable.py`
* `processed_data/forEvaluation/questions/stripQuestionsAndMerge.py`
* `processed_data/forEvaluation/questions/evaluation/E-*.question`

* `processed_data/forEvaluation/mappingWithHuman/*preview/Participant-*`
* `processed_data/forEvaluation/questions/numerified-anonymous/answers`
* `processed_data/forEvaluation/questions/numerified-anonymous-joint/answers`
* `processed_data/forEvaluation/questions/numerified-anonymous-joint/questions`
* `processed_data/forEvaluation/questions/numerified-anonymous-joint/texts`
* `processed_data/forEvaluation/questions/numerified-anonymous/questions`
* `processed_data/forEvaluation/questions/numerified-anonymous/texts`
* `processed_data/forEvaluation/mappingWithHuman/*preview/Participant-10/E-*.csv`
* `processed_data/forEvaluation/questions/numerified-anonymous/answers/E-*.question`
* `processed_data/forEvaluation/questions/numerified-anonymous-joint/answers/E-*.question`
* `processed_data/forEvaluation/questions/numerified-anonymous-joint/questions/E-*.question`
* `processed_data/forEvaluation/questions/numerified-anonymous-joint/texts/E-*.question`
* `processed_data/forEvaluation/questions/numerified-anonymous/*/E-10.question`
* `processed_data/forEvaluation/questions/numerified-anonymous/texts/E-*.question`
* `processed_data/forEvaluation/questions/evaluation/E-*.question`


`processed_data/forEvaluation/mapping/E-*.csv`
`processed_data/forEvaluation/mappingWithExternal/E-*.csv`
`processed_data/forEvaluation/mappingWithHuman/collectMappingWithHumanFiles.py`
`processed_data/forEvaluation/questions/extractCoefficients.py`
`processed_data/forEvaluation/questions/notes.txt`
`processed_data/forEvaluation/questions/prepareFileListForR.py`
`processed_data/forEvaluation/questions/putExternalMeasuresIntoMappingTable.py`
`processed_data/forEvaluation/questions/putReadingMeasuresIntoMappingTable.py`
`processed_data/forEvaluation/questions/stripQuestionsAndMerge.py`
 `get_reading_measures/README-human-18012017`

