import os
import re


# Puts human data into the mapping files.
# The results should be analogous to those of stripQuestionsAndMerge, which does the same for model-generated data.
# It would be sufficient to merge these later in R for each pair

ROOT_FOLDERS = ["/afs/cs.stanford.edu/u/mhahn/mhahn_files/", "/mhahn/repo2/mhahn_files/"]
ROOT_FOLDER = ROOT_FOLDERS[map(lambda x: x[1], filter(lambda x : x[0], zip(map(os.path.isdir, ROOT_FOLDERS), range(0,len(ROOT_FOLDERS)))))[0]]


humanResultsFolder = ROOT_FOLDER+"/ed1516/experiments/corpus/human-results/"
mappingsFolder = ROOT_FOLDER+"/ed1516/experiments/corpus/forEvaluation/mapping/"
outputFolder = ROOT_FOLDER+"/ed1516/experiments/corpus/forEvaluation/mappingWithHuman/"

participants = map(str,range(1,30))
conditions = ["preview","nopreview"]
measures = ["tt","fp","rp","ff"]#,"rb"]#"ro"]#,"sp"]#"rb"]#,"sp"]



humanResults = {}

def makeSureKeyExists(table,key,value):
   if(not(key in table)):
     table[key] = value

for condition in conditions:
   makeSureKeyExists(humanResults,condition,{})
   for measure in measures:
     print("Infile: "+humanResultsFolder+condition+"."+measure)
     inFile = open(humanResultsFolder+condition+"."+measure,"r")
     humanResults[condition][measure] = map(lambda x :re.split(" |\t", x.rstrip()), inFile.readlines())
     inFile.close()


########################
########################

print(humanResults["preview"]["fp"])
#crash()

listOfTextFiles = os.listdir(mappingsFolder)


for condition in conditions:
  outputFolderForCondition = outputFolder+condition+"/"
  if (not( os.path.isdir( outputFolderForCondition))):
    os.mkdir(outputFolderForCondition)
  for participant in participants:
    outputFolderForParticipant = outputFolderForCondition+"/Participant-"+participant
    if(not(os.path.isdir(outputFolderForParticipant))):
       os.mkdir(outputFolderForParticipant)
    for textFile in listOfTextFiles:
      if textFile[0] == "P":
        continue
      textFile = textFile.replace(".csv","")
      textFile = textFile.replace("E-","")
      # now textFile is a string of the item number
      relevantHumanResults = {}
      print("------------"+condition+" "+participant)
      print(textFile)
      startsWithOne = False
      for measure in measures:
        relevantHumanResults[measure] = []
        for i in range(0,len(humanResults[condition][measure])):
          print([participant,textFile+"0"+str(i)])
          newElements = filter(lambda x : x[0] == participant and x[1] == textFile+"0"+str(i), humanResults[condition][measure])
          if len(newElements) == 0 and i == 0:
             startsWithOne = True
             continue
          elif len(newElements) == 0 and i > 0: #sometimes starts at 0 (nopreview), sometimes at 1 (preview)
            break
          if (not (len(newElements) == 1)):
             print ["WARNING 69",len(newElements), participant, textFile+"0"+str(i), map(lambda x:x[:4], newElements), condition,measure]
             print newElements
             assert(len(newElements) == 2) # was apparently redone after calibration, so expect it to occur only twice
          relevantHumanResults[measure] = relevantHumanResults[measure] + [newElements[-1]] # selecting the last one (usually out of one)
#      print("relevantHumanResults")
#      print(relevantHumanResults)
      assert(all(len(relevantHumanResults[measures[0]]) == len(relevantHumanResults[measure]) for measure in measures))

      if len(relevantHumanResults[measures[0]]) == 0:
        continue

      #############################
      #############################

      codingHumanResultsAsOneList = {}
      for measure in measures:
        numberOfScreens = len(relevantHumanResults[measure])
        listOfResults = []
        offset = 1 if startsWithOne else 0
        for i in range(0,numberOfScreens):
           print("Screen "+str(i)+" out of "+str(numberOfScreens))
           resultsForThisScreen = filter(lambda x : x[1].endswith("0"+str(i + offset)), relevantHumanResults[measure])
           assert (len(resultsForThisScreen) > 0), "Error 9096 "+str(len(resultsForThisScreen))
           #print("resultsForThisScreen")
           #print(resultsForThisScreen)
           listOfResults = listOfResults + resultsForThisScreen[0][3:]
        codingHumanResultsAsOneList[measure] = listOfResults
        print("Number of entries: "+measure+"  "+str(len(listOfResults)))
        #print("listOfResults for this measure")
        #print(listOfResults)
      assert(len({len(codingHumanResultsAsOneList[m]) for m in measures}) == 1) # "Not the same number of entries for all measures"

      #print("codingHumanResultsAsOneList")
      #print(codingHumanResultsAsOneList)
      print("Number of items: "+str(len(codingHumanResultsAsOneList['tt'])))


      enrichedFile = open(outputFolderForParticipant+"/E-"+textFile+".csv", "w")
      print(outputFolderForParticipant+"/E-"+textFile+".csv")
      print >> enrichedFile, "\t".join(["HumanPosition","AnonymizedPosition","AnonymizedToken","OriginalToken","ExperimentToken","Participant","Condition","TextNo","JointPosition"]+measures)

      mappingsFile = open(mappingsFolder+"/E-"+textFile+".csv")
      lastHumanNumber = 0
      humanCounter = 0
      lag = 0
      jointPosition = 0
      for mappingLine in mappingsFile:
        mappingLine = mappingLine.rstrip().split("\t")
#        print(mappingLine)
        if len(mappingLine) < 5:
          mappingLine = mappingLine+([""]*(5-len(mappingLine)))
        assert(len(mappingLine) == 5)
        # assume (same assumptions as in stripQuestionsAndMerge.py):
        # 0 position in human texts
        # 1 position in aomyized
        # 2 token in anonymized
        # 3 token in original
        # 4 token in human texts (with some problems at the beginning -- so just ignore the first few tokens for the evaluation)
        jointPosition += 1
        mappingLine = mappingLine + [participant,condition,textFile,str(jointPosition)]
        if mappingLine[0] == "":
           mappingLine = mappingLine + ( [""] * len(measures))
        else:
           numberInHuman = max(1,int(mappingLine[0])) #starts at 1, except for a bug putting in @POSID1 as the token and 0 as the position, which we undo in this step
#           print("NUMBER_IN_HUMAN "+str(numberInHuman))

           humanCounter += 1
           assert(numberInHuman == humanCounter)
#              print(["MISMATCH",numberInHuman,humanCounter])
              
           lastHumanNumber = numberInHuman
           if humanCounter <= len(codingHumanResultsAsOneList[measure]):
             mappingLine = mappingLine + map(lambda measure : codingHumanResultsAsOneList[measure][humanCounter-1], measures)
           else:
             mappingLine = mappingLine+([""]*len(measures))
             print(["MORE",humanCounter,len(codingHumanResultsAsOneList[measure])])
#        print(len(mappingLine))
        assert(len(mappingLine) == 13)
        print >> enrichedFile, "\t".join(mappingLine).replace('"',"-").replace("'","-")
      if humanCounter != len(codingHumanResultsAsOneList[measure]):
           print ["ERROR144", humanCounter, len(codingHumanResultsAsOneList[measure])]
      mappingsFile.close()
      enrichedFile.close()
      
#      if condition == "preview" and participant == "2" and textFile == "2":
 #        crash()

