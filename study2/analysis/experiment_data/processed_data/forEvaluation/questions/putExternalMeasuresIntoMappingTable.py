import os
import re
import string
import nltk
#from nltk.corpus import brown



# Puts external annotation and measures into the mapping files.
# The results should be analogous to those of stripQuestionsAndMerge, which does the same for model-generated data.
# It would be sufficient to merge these later in R for each pair
# Note that "human", "model", and "external" are read separately by the R scripts and then zipped together based on JointPosition.

ROOT_FOLDERS = ["/afs/cs.stanford.edu/u/mhahn/mhahn_files/", "/mhahn/repo2/mhahn_files/"]
ROOT_FOLDER = ROOT_FOLDERS[map(lambda x: x[1], filter(lambda x : x[0], zip(map(os.path.isdir, ROOT_FOLDERS), range(0,len(ROOT_FOLDERS)))))[0]]
#ROOT_FOLDER = "/afs/cs.stanford.edu/u/mhahn/mhahn_files/"


DATASET = "sample" #"eyetrack"
if DATASET == "sample":
  mappingsFolder = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/mapping/"
  outputFolder = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/mappingWithExternal/"
elif DATASET == "eyetrack":
  mappingsFolder = ROOT_FOLDER+"/ed1516/experiments/corpus/forEvaluation/mapping/"
  outputFolder = ROOT_FOLDER+"/ed1516/experiments/corpus/forEvaluation/mappingWithExternal/"
  LOCATION_OF_ANONYMIZED_FILES = ROOT_FOLDER+"/ed1516/experiments/corpus/forEvaluation/questions/evaluation/"
else:
  assert False

ptb2UniversalFile = open("en-ptb.map","r")
ptb2Universal = dict(map(lambda x : (x[0],x[1]), filter(lambda x : len(x) > 1, map(lambda x : x.rstrip().split("\t"), ptb2UniversalFile.readlines()))))
ptb2UniversalFile.close()




wordFreqFile = "/afs/cs.stanford.edu/u/mhahn/scr/num2CharsNoEntities-cnn-freqs"
with open(wordFreqFile) as wFreqF:
#   freqsL = wFreqF.read().split("\n")
   wordFreqs = {}
   for line in wFreqF:
     line = line.rstrip().split(" ")
     if len(line) < 3:
       continue
     assert(not(line[1] in wordFreqs))
     wordFreqs[line[1]] = int(line[2])


#freq_brown = nltk.FreqDist(brown.words())
#cfreq_brown_2gram = nltk.ConditionalFreqDist(nltk.bigrams(brown.words()))
#prob2Gram = nltk.ConditionalProbDist(cfreq_brown_2gram, nltk.MLEProbDist)

def makeSureKeyExists(table,key,value):
   if(not(key in table)):
     table[key] = value

listOfTextFiles = os.listdir(mappingsFolder)

HEADER = ["HumanPosition","AnonymizedPosition","AnonymizedToken","OriginalToken","ExperimentToken","JointPosition","ExperimentTokenLength","IsNamedEntity","OccursInQuestion", "IsCorrectAnswer","POS.PTB","POS.Universal","PositionInHumanSentence", "OccurrencesOfCorrectAnswerSoFar","IsFirstInLine", "WordFreq"]



for textFile in listOfTextFiles:
      if textFile == ".svn":
        continue
      if textFile[0] == "P":
        continue
      enrichedFile = open(outputFolder+"/"+textFile, "w")
      print(outputFolder+"/"+textFile)
      print >> enrichedFile, "\t".join(HEADER)
      # read the question
      if DATASET == "sample":
        anonymizedFileLocation = textFile.replace("_","/").replace(".csv",".question")
      elif DATASET == "eyetrack":
        anonymizedFileLocation = LOCATION_OF_ANONYMIZED_FILES+(textFile.replace(".csv",".question"))
      else:
        assert False
      anonymizedFile = open(anonymizedFileLocation)
      anonymizedData = map(lambda x : x.rstrip(), anonymizedFile.readlines())
      anonymizedFile.close()
      #print(anonymizedData)
      assert(anonymizedData[1] == "")
      assert(anonymizedData[3] == "")
      assert(anonymizedData[5] == "")
      assert(anonymizedData[7] == "")
      question = anonymizedData[4].split(" ")
      text = anonymizedData[2].split(" ")
      answerOccurrences = map(str, filter(lambda x: text[x] == anonymizedData[6], range(0,len(text))))
      #print(answerOccurrences)




      mappingsFile = open(mappingsFolder+"/"+textFile, "r")
      mappingsList = map(lambda x : x.rstrip().split("\t"), mappingsFile.readlines())




      tokenizedData = filter(lambda x : x != "", map(lambda y : y[3], mappingsList))
      #print(tokenizedData)
      tagged = nltk.pos_tag(tokenizedData)
      #print(tagged)
#      crash()


      humanPositionInSentence = 0 #counts human tokens

      countInOriginal = 0#0-based
      jointPosition = 0 #1-based
      occurrencesOfCorrectAnswerSoFar = 0
      characterCountSoFar = 0
      for mappingLine in mappingsList:
        if len(mappingLine) < 5:
          mappingLine = mappingLine+([""]*(5-len(mappingLine)))
        assert(len(mappingLine) == 5)





        jointPosition += 1
        mappingLine.append(str(jointPosition))

        

        ####
        experimentToken = mappingLine[4]
        if experimentToken != "":
          experimentTokenLength = str(len(experimentToken.translate(string.maketrans("",""), string.punctuation)))
        else:
          experimentTokenLength = ""
        mappingLine.append(experimentTokenLength)

        ###
        anonymizedToken = mappingLine[2]
        if anonymizedToken.startswith("@entity"):
           isNamedEntity = "TRUE"
        elif len(anonymizedToken) > 0:
           isNamedEntity = "FALSE"
        else:
           assert(len(isNamedEntity) > 0) # take previous one in case there is no new anonymized token here
           #isNamedEntity = ""
        mappingLine.append(isNamedEntity)

        ###
        anonymizedToken = mappingLine[2]
        if anonymizedToken in question:
           occursInQuestion = "TRUE"
        elif anonymizedToken != "":
           occursInQuestion = "FALSE"
        else:
           assert(len(occursInQuestion) > 0)
           #occursInQuestion = ""
        mappingLine.append(occursInQuestion)

        ###
        anonymizedPosition = mappingLine[1]
        if anonymizedPosition in answerOccurrences:
           isCorrectAnswer = "TRUE"
           occurrencesOfCorrectAnswerSoFar += 1
        elif anonymizedToken != "": #isNamedEntity == "TRUE":
           isCorrectAnswer = "FALSE"
        else:
           assert(len(isCorrectAnswer) > 0)
           #isCorrectAnswer = ""
        mappingLine.append(isCorrectAnswer)

        ###
        if mappingLine[3] != "":
          tokenAndTag = tagged[countInOriginal]
          assert(tokenAndTag[0] == mappingLine[3])
          tag = tokenAndTag[1]
          universalTag = ptb2Universal[tag]
          countInOriginal += 1#after this increment: 1-based
        else:
          tag = ""
          universalTag = ""
        mappingLine.append(tag)
        mappingLine.append(universalTag)

        ####
        if mappingLine[2] in [".","?","!"]:
           humanPositionInSentence = 0
        if mappingLine[0] != "":
           humanPositionInSentence +=1
           reportedHumanPositionInSentence = str(humanPositionInSentence)
        else:
           reportedHumanPositionInSentence = ""
        mappingLine.append(reportedHumanPositionInSentence)
        
        ####
        mappingLine.append(str(occurrencesOfCorrectAnswerSoFar))

        ####
        if mappingLine[0] != "":
          if mappingLine[4] == "":
            #mappingLine[4] = mappingLine[3] #hacky way
            print("WARNING PUTTING IN WORD: 166: "+mappingLine[4])
          if characterCountSoFar + len(mappingLine[4]) + 1 <= 85:
             characterCountSoFar += len(mappingLine[4]) + 1
             isFirstInLine = "FALSE"
          else:
             characterCountSoFar = len(mappingLine[4])
             isFirstInLine = "TRUE"
        else:
          isFirstInLine = ""
        mappingLine.append(isFirstInLine)


        if anonymizedToken != "" and anonymizedToken.lower() in wordFreqs:
           wordFreq = str(wordFreqs[anonymizedToken.lower()]+1) # smooth, just in case
      #  elif anonymizedToken != "":
       #    wordFreq = "1" # th cutoff for the word list is 100
        else:
           wordFreq = ""
        mappingLine.append(wordFreq)

        assert(len(HEADER) == len(mappingLine))
        print >> enrichedFile, "\t".join(mappingLine).replace('"',"-").replace("'","-")
      mappingsFile.close()
      enrichedFile.close()

