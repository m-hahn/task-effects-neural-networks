with open("mappingWithExternal.tsv", "r") as inFile:
   mapping = [x.split("\t") for x in inFile.read().strip().split("\n")]
   header = mapping[0]
   mapping = [dict(list(zip(header, x))) for x in mapping[1:]]
with open("../../analysis_with_NEAT/neat_predicts_human/averaged-NEAT-predictions_CharPreview.tsv", "r") as inFile:
   deanon = [[z.strip('"') for z in x.split("\t")] for x in inFile.read().strip().split("\n")]
   assert len(deanon[1]) == len(deanon[0])+1 # rownum
   header = [x.strip('"') for x in deanon[0]]
   deanon = [dict(list(zip(header, x[1:]))) for x in deanon[1:]]

texts = sorted(list(set([x["TextFileName"] for x in mapping])))

print(mapping[:10])
print(deanon[:10])
print(texts)
with open(f"{__file__}.tsv", "w") as outFile:
 print("\t".join(["TextFileName", "JointPosition", "OriginalToken", "Position_Deanonymized_NEAT", "Word_Deanonymized_NEAT"]), file=outFile)
 for text in texts:
   mapping_ = [x for x in mapping if x["TextFileName"] == text]
   deanon_ = [x for x in deanon if x["Text"].strip('"') == text and x["Condition"].strip('"') == "Preview"]
   #print(mapping_[:3])
   #print(deanon_[:3])
   i = 0
   j = 0
   while i < len(mapping_) and j < len(deanon_):
     #print("...")
   #  if mapping_[i]["TextFileName"] == "TE-4":
     print(i, mapping_[i]["OriginalToken"], deanon_[j]["Word"],mapping_[i]["TextFileName"], "JOINT", mapping_[i]["JointPosition"], "INDICES", i, j)
     EXCEPTION = False
     # Mismatch happens when a single named entity has different forms in the text -- due to the data processing pipeline, the model uses the normalized form created by the NE tagger. This primarily happens to a few named entities that are variably named by surname or full name in the texts.
     if mapping_[i]["TextFileName"] == "TE-1" and mapping_[i]["JointPosition"] == "32":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "…" and deanon_[j]["Word"] == ".":
       EXCEPTION = True
     elif mapping_[i]["OriginalToken"] == "Raheem" and deanon_[j]["Word"] == "Sterling":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Atletico" and deanon_[j]["Word"] == "Paranaense":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Gabriel" and deanon_[j]["Word"] == "Rasch":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Dana" and deanon_[j]["Word"] == "Mattioli":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Wall" and deanon_[j]["Word"] == "Journal":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Street" and deanon_[j]["Word"] == "Journal":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Chris" and deanon_[j]["Word"] == "Froome":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Valverde" and deanon_[j]["Word"] == "Alejandro":
       j += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Matt" and deanon_[j]["Word"] == "Khouw":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Barclays" and deanon_[j]["Word"] == "Premier":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Todd" and deanon_[j]["Word"] == "Phillips":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Angela" and deanon_[j]["Word"] == "Moore":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Francesco" and deanon_[j]["Word"] == "Dracone":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "John" and deanon_[j]["Word"] == "Foran" and mapping_[i]["JointPosition"] == "186":
       EXCEPTION = True
     elif mapping_[i]["OriginalToken"] == "John" and deanon_[j]["Word"] == "Foran":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Foods" and deanon_[j]["Word"] == "store":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Marvin" and deanon_[j]["Word"] == "Jones":
       i += 1
       continue
     elif mapping_[i]["OriginalToken"] == "King" and deanon_[j]["Word"] == "B.B.":
       EXCEPTION = True
     elif mapping_[i]["OriginalToken"] == "Thibault" and deanon_[j]["Word"] == "Thibault-Lecuivre":
       EXCEPTION = True
     elif mapping_[i]["OriginalToken"] == "French" and deanon_[j]["Word"] == "France":
       EXCEPTION = True
     elif mapping_[i]["OriginalToken"] == "Blues" and deanon_[j]["Word"] == "Bluesman":
       EXCEPTION = True
     elif mapping_[i]["OriginalToken"] == "Korea" and deanon_[j]["Word"] == "Korean":
       EXCEPTION = True
     elif mapping_[i]["OriginalToken"] == "U.S." and deanon_[j]["Word"] == "USA":
       EXCEPTION = True
     elif mapping_[i]["OriginalToken"] == "370" and deanon_[j]["Word"] == "MH370":
       EXCEPTION = True
     elif mapping_[i]["OriginalToken"] == "Coulibaly" and deanon_[j]["Word"] == "Amedy":
       j += 1
       continue
     elif mapping_[i]["OriginalToken"] == "Chinese" and deanon_[j]["Word"] == "China":
       EXCEPTION = True
     elif mapping_[i]["OriginalToken"] == "Brazilian" and deanon_[j]["Word"] == "Brazilians":
       EXCEPTION = True
     elif mapping_[i]["OriginalToken"] == "," and deanon_[j]["Word"] == "Foran" and mapping_[i]["JointPosition"] == "98":
       j += 1
     elif mapping_[i]["OriginalToken"] == "was" and deanon_[j]["Word"] == "Froome" and mapping_[i]["JointPosition"] == "163":
       j += 1
     elif mapping_[i]["OriginalToken"] == "cemetary" and deanon_[j]["Word"] == "cemetery": # typo -- this part wasn't part of the experiments, so no impact on human data
       EXCEPTION = True
     elif mapping_[i]["OriginalToken"] == "intital" and deanon_[j]["Word"] == "initial": # typo -- this part wasn't part of the experiments, so no impact on human data
       EXCEPTION = True
     elif mapping_[i]["TextFileName"] == "TE-17" and mapping_[i]["JointPosition"] == "21":
        i += 4
        continue
     elif mapping_[i]["TextFileName"] == "TE-17" and mapping_[i]["JointPosition"] == "87":
        i += 4
        continue
     elif mapping_[i]["TextFileName"] == "TE-8" and mapping_[i]["JointPosition"] == "286" and i == 285 and j == 280:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-8" and mapping_[i]["JointPosition"] == "227" and i == 226 and j == 221:
        EXCEPTION = True
     elif mapping_[i]["TextFileName"] == "TE-18" and  mapping_[i]["JointPosition"] in ["200", "264"]:
       EXCEPTION = True
     elif mapping_[i]["TextFileName"] == "TE-19" and  mapping_[i]["JointPosition"] in ["30"] and j == 28:
        j += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-19" and  mapping_[i]["JointPosition"] in ["16"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-19" and  mapping_[i]["JointPosition"] in ["110", "120"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-19" and  mapping_[i]["JointPosition"] in ["189"] and j == 186:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-19" and  mapping_[i]["JointPosition"] in ["197"] and j == 193:
        i += 1
        continue
#     elif mapping_[i]["TextFileName"] == "TE-19" and  mapping_[i]["JointPosition"] in ["30"]:
 #       i += 1
  #      continue
     elif mapping_[i]["TextFileName"] == "TE-2" and  mapping_[i]["JointPosition"] in ["21", "22", "23", "24"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-2" and  mapping_[i]["JointPosition"] in ["25"]:
        EXCEPTION = True
     elif mapping_[i]["TextFileName"] == "TE-20" and  mapping_[i]["JointPosition"] in ["7"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-3" and  mapping_[i]["JointPosition"] in ["3"] and j == 2:
        j += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-3" and  mapping_[i]["JointPosition"] in ["14", "15"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-3" and  mapping_[i]["JointPosition"] in ["67"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-3" and  i == 120 and j == 118:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-3" and  i == 165 and j == 161:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-3" and  i == 164 and j == 161:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-3" and  i == 163 and j == 160:
        EXCEPTION = True
     elif mapping_[i]["TextFileName"] == "TE-3" and  i == 121 and j == 118:
        EXCEPTION = True
     elif mapping_[i]["TextFileName"] == "TE-20" and  mapping_[i]["JointPosition"] in ["551"] and j == 555:
        j += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-20" and  mapping_[i]["JointPosition"] in ["531"] and j == 534:
        j += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-20" and  mapping_[i]["JointPosition"] in ["526"] and j == 528:
        j += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-20" and  mapping_[i]["JointPosition"] in ["489"] and j == 490:
        j += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-20" and  mapping_[i]["JointPosition"] in ["487"] and j == 487:
        j += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-20" and  mapping_[i]["JointPosition"] in ["380"] and j == 379:
        j += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-20" and  mapping_[i]["JointPosition"] in ["234"] and j == 234:
        i += 1
#        assert False
        continue
     elif mapping_[i]["TextFileName"] == "TE-20" and  mapping_[i]["JointPosition"] in ["229"] and j == 228:
        j += 1
#        assert False
        continue
     elif mapping_[i]["TextFileName"] == "TE-20" and  mapping_[i]["JointPosition"] in ["128"] and j == 126:
        j += 1
#        assert False
        continue
     elif mapping_[i]["TextFileName"] == "TE-4" and  mapping_[i]["JointPosition"] in ["37"]:
        EXCEPTION = True
     elif mapping_[i]["TextFileName"] == "TE-4" and  mapping_[i]["JointPosition"] in ["181"] and j ==176:
        j += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-4" and  mapping_[i]["JointPosition"] in ["152"] and j ==146:
        j += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-4" and  mapping_[i]["JointPosition"] in ["91"] and j == 87:
        j += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-4" and  mapping_[i]["JointPosition"] in ["99", "100", "101"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-4" and  mapping_[i]["JointPosition"] in ["99"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-4" and  mapping_[i]["JointPosition"] in ["38", "39"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-4" and  mapping_[i]["JointPosition"] in ["26"] and j == 23:
        j += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-4" and  mapping_[i]["JointPosition"] in ["12", "13"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-5" and  mapping_[i]["JointPosition"] in ["7"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-5" and  mapping_[i]["JointPosition"] in ["24"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-5" and  mapping_[i]["JointPosition"] in ["186"]:
        EXCEPTION = True
     elif mapping_[i]["TextFileName"] == "TE-5" and  mapping_[i]["JointPosition"] in ["183", "184", "185"]:
        print("JUMP 183")
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-6" and  mapping_[i]["JointPosition"] in ["326"] and j == 325:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-7" and  mapping_[i]["JointPosition"] in ["42", "43"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-8" and  mapping_[i]["JointPosition"] in ["45"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-8" and  mapping_[i]["JointPosition"] in ["79", "81", "127", "207"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-8" and  mapping_[i]["JointPosition"] in ["227"]:
        j += 1
        assert False
        continue
     elif mapping_[i]["TextFileName"] == "TE-9" and  mapping_[i]["JointPosition"] in ["130", "131", "132"]:
        i += 1
        continue
     elif mapping_[i]["TextFileName"] == "TE-9" and  mapping_[i]["JointPosition"] in ["133"]:
        EXCEPTION = True

     if mapping_[i]["OriginalToken"].lower().replace("’", "-").replace("–", "-") != deanon_[j]["Word"].lower().replace("'", "-").replace('"', "-"):
      if EXCEPTION or mapping_[i]["OriginalToken"].lower() == "-":
         pass
      else:
         assert False, (mapping_[i]["OriginalToken"],  deanon_[j]["Word"])
     print("\t".join([mapping_[i]["TextFileName"], mapping_[i]["JointPosition"], mapping_[i]["OriginalToken"], deanon_[j]["Position"], deanon_[j]["Word"]]), file=outFile)
#     if mapping_[i]["TextFileName"] == "TE-5":
 #      print("\t".join([mapping_[i]["TextFileName"], mapping_[i]["JointPosition"], mapping_[i]["OriginalToken"], deanon_[j]["Position"], deanon_[j]["Word"]]))
     i += 1
     j += 1
     # issue relating to the Named Entity identification and normalization

   #quit()
