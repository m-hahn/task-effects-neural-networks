import os

BASE_DIR = "/afs/cs.stanford.edu/u/mhahn/scr/deepmind-qa/joint/training/noentities-sample/"


#0 get file list
fileList = os.listdir(BASE_DIR+"mapping/")
if "merged.csv" in fileList:
  fileList.remove("merged.csv")

def processToPleaseR(row):
  return map(lambda x:x.replace("#","HASHTAG"),row)

#1 Merge predictions for each model (for now attention, should be extended to language modeling)
modelList = os.listdir(BASE_DIR+"annotation-textsonly/")
for model in modelList:
 if "langmod" in model:
   print("Skipping langmod files.")
   continue
 if os.path.isdir(BASE_DIR+"annotation-textsonly/"+model):
   with open(BASE_DIR+"annotation-textsonly/"+model+"/"+"merged.csv","w") as outStream:
     print(BASE_DIR+"annotation-textsonly/"+model+"/"+"merged.csv")
     print >> outStream, "\t".join(["HumanPosition","AnonymizedPosition","AnonymizedToken","OriginalToken",   "ExperimentToken", "LuaID",   "AttentionDecision",       "AttentionScore",  "JointPosition"]+["Dataset","TextID"])
     for fileName in fileList:
       if (not os.path.isfile(BASE_DIR+"annotation-textsonly/"+model+"/"+fileName)):
          print("ERROR: file has not annotation")
          continue
       else:
         print(fileName)

       with open(BASE_DIR+"annotation-textsonly/"+model+"/"+fileName,"r") as inStream:
        fileContent = inStream.read().split("\n")
        fileContent = fileContent[1:]
        fileContent = filter(lambda x:len(x) > 0, fileContent)
        fileContent = map(lambda x:x+"\t"+("cnn" if "cnn" in fileName else "dailymail")+"\t"+fileName, fileContent)
        fileContent = processToPleaseR(fileContent)
        for row in fileContent:
          print >> outStream, row

#2 Merge the mapping file
header = ["HumanPosition",   "AnonymizedPosition", "AnonymizedToken", "OriginalToken",   "ExperimentToken", "JointPosition",   "ExperimentTokenLength",   "IsNamedEntity"   ,"OccursInQuestion",        "IsCorrectAnswer", "POS.PTB", "POS.Universal",   "PositionInHumanSentence", "OccurrencesOfCorrectAnswerSoFar", "IsFirstInLine"]
with open(BASE_DIR+"mappingWithExternal/"+"merged.csv","w") as outStream:
     print >> outStream, "\t".join(header+["TextID"])
     for fileName in fileList:
       with open(BASE_DIR+"mappingWithExternal/"+fileName,"r") as inStream:
        fileContent = inStream.read().split("\n")
        fileContent = fileContent[1:]
        fileContent = filter(lambda x:len(x) > 0, fileContent)
        fileContent = map(lambda x:x+"\t"+fileName, fileContent)
        fileContent = processToPleaseR(fileContent)
        for row in fileContent:
          print >> outStream, row


