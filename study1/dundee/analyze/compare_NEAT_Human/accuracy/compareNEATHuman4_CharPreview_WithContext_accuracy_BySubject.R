library(tidyr)
library(dplyr)


human = read.csv("BASE/STUDY1/Dundee/ReadingAndPOS.csv", sep="\t")
MODEL = "MERGED"
data = read.csv(paste("BASE/STUDY1/Dundee-annotated/run_attention4_CharPreview_WithContext_dundee.py_", MODEL, sep=""), sep="\t")

params = read.csv("../../../../python/attention/logs/collectLogs_train_attention4_CharPreview_WithContext.py.tsv", sep="\t")


data = merge(data, params, by=c("ModelID"))

data = data %>% filter(alpha == 3, Epochs>1)


#data = data %>% filter(!(Token %in% c(".", ",", "!", "?")))
#data = data %>% filter(!(Token %in% c("n't", "'d", "'s", 'll', 're', 'clock')))

data2 = data[(2:nrow(data)),]
data3 = data[(1:nrow(data)-1),]
unique(data2[data2$WNUM == data3$WNUM,]$Token)

data2$DUPLICATED = FALSE
duplicated = data2[data2$WNUM == data3$WNUM,]
data2[data2$WNUM == data3$WNUM,]$DUPLICATED = TRUE

data$DUPLICATED = c(FALSE, data2$DUPLICATED)

data = data %>% filter(!DUPLICATED)

rows = data %>% group_by(PositionModel, Token, Itemno, WNUM, ID, InVocab) %>% summarise(rows=NROW(Attended))
print(paste("ASSERT", max(rows$rows) == min(rows$rows), "\n"))

data = data %>% group_by(PositionModel, Token, Itemno, WNUM, ID, InVocab) %>% summarise(AttProbability = mean(AttProbability), Attended = mean(Attended))


surprisal = read.csv("BASE/STUDY1/Dundee-annotated/run_lm_dundee.py_42636007", sep="\t", header=FALSE)
names(surprisal) = c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab", "Surprisal")
data = merge(data, surprisal, by=c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab"))



wordfreq = read.csv("vocabularies/cnn_nonAnonymized_50000.txt", sep="\t", header=FALSE, quote=NULL)
names(wordfreq) = c("WordID", "Token", "WordFreq")

data = merge(data, wordfreq, by=c("Token"))

data = data %>% filter(InVocab == "InVocab") %>% group_by(Itemno, WNUM) %>% summarise(AttProbability = max(AttProbability), WordFreq=min(WordFreq), Surprisal=sum(Surprisal))
data$LogWordFreq = log(data$WordFreq)

data = merge(human, data, by=c("Itemno", "WNUM"), all=TRUE)
data$WordLength = unlist(lapply(as.character(data$WORD), FUN=nchar))
library(lme4)
data = data %>% mutate(ItemID = paste0(Itemno, "_", WNUM))
data$FIXATED = (data$FPASSD > 0)

#summary(lmer(FPASSD ~ WordLength + WNUM + LogWordFreq + AttProbability + Surprisal + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED)))

data = data %>% mutate(WordLength.C = (WordLength-mean(WordLength, na.rm=TRUE))/sd(WordLength, na.rm=TRUE))
data = data %>% mutate(LogWordFreq.C = (LogWordFreq-mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
data = data %>% mutate(WNUM.C = (WNUM-mean(WNUM, na.rm=TRUE))/sd(WNUM, na.rm=TRUE))
data = data %>% mutate(AttProbability.C = (AttProbability-mean(AttProbability, na.rm=TRUE))/sd(AttProbability, na.rm=TRUE))
data = data %>% mutate(Surprisal.C = (Surprisal-mean(Surprisal, na.rm=TRUE))/sd(Surprisal, na.rm=TRUE))


data$Surprisal.Resid = residuals(lm(Surprisal ~ LogWordFreq, data=data, na.action=na.exclude))
data$AttProbability.Resid = residuals(lm(AttProbability ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + Surprisal, data=data, na.action=na.exclude))




calculateAccuracy = function(data, predictor, sign) {
   data = data %>% group_by(SUBJ) %>% filter(!is.na(FIXATED))
   data$predictor = data[[predictor]]
   data = data %>% filter(!is.na(predictor))

   data2 = data %>% group_by(SUBJ) %>% summarise(cutoff_LogWordFreq = quantile(sign*predictor, 0.62, na.rm=TRUE))
   data = merge(data, data2, by=c("SUBJ"))
   data$Predicted = (sign*data$predictor < data$cutoff_LogWordFreq)
   data = data %>% summarise(accuracy = mean(data$FIXATED == Predicted, na.rm=TRUE), precision_fix = sum(Predicted * FIXATED)/sum(Predicted), recall_fix = sum(Predicted * FIXATED) / sum(FIXATED), precision_skip = sum((1-Predicted) * (1-FIXATED))/sum((1-Predicted)), recall_skip = sum((1-Predicted)*(1-FIXATED))/sum((1-FIXATED)))


   data = data %>% mutate(f1_fix = 2*(precision_fix*recall_fix)/(precision_fix+recall_fix))
   data = data %>% mutate(f1_skip = 2*(precision_skip*recall_skip)/(precision_skip+recall_skip))

   data = data %>% group_by() %>% summarise(accuracy = mean(accuracy), f1_fix = mean(f1_fix), f1_skip = mean(f1_skip))

   accuracy = data$accuracy[[1]]
   f1_fix = data$f1_fix[[1]]
   f1_skip = data$f1_skip[[1]]
   print(paste(predictor, " ", accuracy, " ", f1_fix, " ", f1_skip, "\n"))
}



calculateAccuracy(data, "AttProbability", -1)
calculateAccuracy(data, "LogWordFreq", 1)
calculateAccuracy(data, "WordLength",-1)
calculateAccuracy(data, "Surprisal", -1)


data$RandomUniform = runif(nrow(data))
calculateAccuracy(data, "RandomUniform",1)


# Now per human subject
data_ = data.frame()
for(subject in unique(data$SUBJ)) {
  averages = data %>% filter(SUBJ != subject) %>% group_by(WNUM, WORD) %>% summarise(HumanFixationRate=mean(FIXATED, na.rm=TRUE))
  averageForReader = data %>% filter(SUBJ == subject) %>% summarise(HumanFixationRateMean=mean(FIXATED, na.rm=TRUE))
  averages$HumanFixationRate = ifelse(is.na(averages$HumanFixationRate), averageForReader$HumanFixationRateMean[[1]], averages$HumanFixationRate)
  averages = merge(averages, data %>% filter(SUBJ == subject), by=c("WNUM", "WORD"))
  data_ = rbind(averages, data_)
}
calculateAccuracy(data_, "HumanFixationRate", -1)



