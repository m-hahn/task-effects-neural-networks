library(tidyr)
library(dplyr)

human = read.csv("BASE/STUDY1/Dundee/ReadingAndPOS.csv", sep="\t")
data = read.csv("BASE/STUDY1/Dundee-annotated/run_lm_noised_dundee_fullOrRandom.py_RANDOM_86850892", sep="\t") %>% rename(PositionModel=Position)

wordfreq = read.csv("../../../../python/vocabularies/cnn_nonAnonymized_50000.txt", sep="\t", header=FALSE, quote=NULL)
names(wordfreq) = c("WordID", "Token", "WordFreq")

data = merge(data, wordfreq, by=c("Token"), all.x=TRUE)

data = data %>% filter(Surprisal_OOV_Status == "InVocab") %>% group_by(Itemno, WNUM) %>% summarise(WordFreq=min(WordFreq), Surprisal=sum(Surprisal))
data$LogWordFreq = log(data$WordFreq)

data = merge(human, data, by=c("Itemno", "WNUM"), all=TRUE)
data$WordLength = unlist(lapply(as.character(data$WORD), FUN=nchar))
library(lme4)
data = data %>% mutate(ItemID = paste0(Itemno, "_", WNUM))
data$FIXATED = (data$FPASSD > 0)

#summary(lmer(FPASSD ~ WordLength + WNUM + LogWordFreq + AttProbability + Surprisal + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED)))

data = data %>% mutate(WordLength = (WordLength-mean(WordLength, na.rm=TRUE))/sd(WordLength, na.rm=TRUE))
data = data %>% mutate(LogWordFreq = (LogWordFreq-mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
data = data %>% mutate(WNUM.C = (WNUM-mean(WNUM, na.rm=TRUE))/sd(WNUM, na.rm=TRUE))


data$Surprisal.Resid = residuals(lm(Surprisal ~ LogWordFreq, data=data, na.action=na.exclude))

sink("fpassd_random.txt")
model0 = (lmer(FPASSD ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED), REML=FALSE))
model1 = (lmer(FPASSD ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED), REML=FALSE))
print(summary(model0))
print(summary(model1))
anova(model0, model1)
sink()


sink("ffix_random.txt")
model0 = (lmer(FFIXDUR ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED), REML=FALSE))
model1 = (lmer(FFIXDUR ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED), REML=FALSE))
print(summary(model0))
print(summary(model1))
anova(model0, model1)
sink()

sink("totdur_random.txt")
model0 = (lmer(TOTDUR ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + (1|SUBJ) + (1|ItemID), data=data %>% filter(TOTDUR>0), REML=FALSE))
model1 = (lmer(TOTDUR ~ WordLength*LogWordFreq + WNUM.C*LogWordFreq + WNUM.C*WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid + (1|SUBJ) + (1|ItemID), data=data %>% filter(TOTDUR>0), REML=FALSE))
print(summary(model0))
print(summary(model1))
anova(model0, model1)
sink()


