Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: TOTDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + (1 | SUBJ) +      (1 | ItemID)
   Data: data %>% filter(TOTDUR > 0)

     AIC      BIC   logLik deviance df.resid 
 3414895  3415000 -1707438  3414875   269596 

Scaled residuals: 
   Min     1Q Median     3Q    Max 
-4.730 -0.552 -0.172  0.315 40.025 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  2012.8   44.86  
 SUBJ     (Intercept)   680.2   26.08  
 Residual             17003.1  130.40  
Number of obs: 269606, groups:  ItemID, 47384; SUBJ, 10

Fixed effects:
                       Estimate Std. Error t value
(Intercept)            244.0121     8.2575  29.550
WordLength              34.8524     0.4847  71.909
LogWordFreq             -5.5716     0.4563 -12.211
WNUM.C                  -2.8907     0.3427  -8.434
WordLength:LogWordFreq  -0.1392     0.3580  -0.389
LogWordFreq:WNUM.C       0.2100     0.4551   0.461
WordLength:WNUM.C       -2.2262     0.4644  -4.794

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C WL:LWF LWF:WN
WordLength   0.006                                   
LogWordFreq  0.004  0.617                            
WNUM.C       0.000  0.003  0.014                     
WrdLngt:LWF  0.026  0.305 -0.044 -0.007              
LgWF:WNUM.C  0.000  0.004  0.011  0.120 -0.003       
WrdL:WNUM.C  0.000  0.006  0.005 -0.032 -0.004  0.665
convergence code: 0
Model failed to converge with max|grad| = 0.00207227 (tol = 0.002, component 1)

Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: TOTDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid +      (1 | SUBJ) + (1 | ItemID)
   Data: data %>% filter(TOTDUR > 0)

     AIC      BIC   logLik deviance df.resid 
 3414297  3414412 -1707137  3414275   269595 

Scaled residuals: 
   Min     1Q Median     3Q    Max 
-4.678 -0.552 -0.171  0.316 39.979 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  1949.3   44.15  
 SUBJ     (Intercept)   681.2   26.10  
 Residual             17000.8  130.39  
Number of obs: 269606, groups:  ItemID, 47384; SUBJ, 10

Fixed effects:
                       Estimate Std. Error t value
(Intercept)            243.8030     8.2634  29.504
WordLength              33.3690     0.4851  68.794
LogWordFreq             -6.5707     0.4550 -14.442
WNUM.C                  -2.8789     0.3406  -8.453
Surprisal.Resid          3.7290     0.1516  24.593
WordLength:LogWordFreq  -0.2108     0.3555  -0.593
LogWordFreq:WNUM.C       0.1749     0.4519   0.387
WordLength:WNUM.C       -2.2552     0.4610  -4.892

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C Srpr.R WL:LWF LWF:WN
WordLength   0.006                                          
LogWordFreq  0.004  0.620                                   
WNUM.C       0.000  0.003  0.014                            
Surprsl.Rsd -0.001 -0.126 -0.090  0.001                     
WrdLngt:LWF  0.025  0.303 -0.044 -0.007 -0.009              
LgWF:WNUM.C  0.000  0.004  0.011  0.121 -0.003 -0.003       
WrdL:WNUM.C  0.000  0.006  0.005 -0.033 -0.003 -0.004  0.665
Data: data %>% filter(TOTDUR > 0)
Models:
model0: TOTDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C * 
model0:     WordLength + WordLength + WNUM.C + LogWordFreq + (1 | SUBJ) + 
model0:     (1 | ItemID)
model1: TOTDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C * 
model1:     WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid + 
model1:     (1 | SUBJ) + (1 | ItemID)
       Df     AIC     BIC   logLik deviance Chisq Chi Df Pr(>Chisq)    
model0 10 3414895 3415000 -1707438  3414875                            
model1 11 3414297 3414412 -1707137  3414275 600.4      1  < 2.2e-16 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
