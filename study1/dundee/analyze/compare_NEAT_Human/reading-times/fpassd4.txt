Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: FPASSD ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + (1 | SUBJ) +      (1 | ItemID)
   Data: data %>% filter(FIXATED)

     AIC      BIC   logLik deviance df.resid 
 2803241  2803344 -1401610  2803221   226869 

Scaled residuals: 
   Min     1Q Median     3Q    Max 
-5.149 -0.546 -0.149  0.331 47.706 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  1483.0   38.51  
 SUBJ     (Intercept)   603.8   24.57  
 Residual             12426.5  111.47  
Number of obs: 226879, groups:  ItemID, 46190; SUBJ, 10

Fixed effects:
                       Estimate Std. Error t value
(Intercept)            233.1533     7.7795  29.970
WordLength              32.2892     0.4371  73.865
LogWordFreq             -3.6334     0.4142  -8.771
WNUM.C                  -2.2514     0.3149  -7.150
WordLength:LogWordFreq   0.3081     0.3235   0.952
LogWordFreq:WNUM.C      -0.4942     0.4117  -1.200
WordLength:WNUM.C       -2.5127     0.4175  -6.018

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C WL:LWF LWF:WN
WordLength   0.005                                   
LogWordFreq  0.004  0.598                            
WNUM.C       0.000  0.003  0.011                     
WrdLngt:LWF  0.024  0.311 -0.072 -0.005              
LgWF:WNUM.C  0.000  0.005  0.011  0.136 -0.003       
WrdL:WNUM.C  0.000  0.008  0.007 -0.059 -0.004  0.657
Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: FPASSD ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid +      (1 | SUBJ) + (1 | ItemID)
   Data: data %>% filter(FIXATED)

     AIC      BIC   logLik deviance df.resid 
 2802734  2802848 -1401356  2802712   226868 

Scaled residuals: 
   Min     1Q Median     3Q    Max 
-5.057 -0.546 -0.149  0.331 47.657 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  1439.7   37.94  
 SUBJ     (Intercept)   604.3   24.58  
 Residual             12424.0  111.46  
Number of obs: 226879, groups:  ItemID, 46190; SUBJ, 10

Fixed effects:
                       Estimate Std. Error t value
(Intercept)            232.9863     7.7820  29.939
WordLength              31.0578     0.4379  70.920
LogWordFreq             -4.4592     0.4134 -10.786
WNUM.C                  -2.2400     0.3132  -7.153
Surprisal.Resid          3.0907     0.1366  22.631
WordLength:LogWordFreq   0.2441     0.3216   0.759
LogWordFreq:WNUM.C      -0.5202     0.4093  -1.271
WordLength:WNUM.C       -2.5445     0.4148  -6.133

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C Srpr.R WL:LWF LWF:WN
WordLength   0.005                                          
LogWordFreq  0.004  0.601                                   
WNUM.C       0.000  0.003  0.011                            
Surprsl.Rsd -0.001 -0.127 -0.089  0.002                     
WrdLngt:LWF  0.023  0.310 -0.072 -0.005 -0.010              
LgWF:WNUM.C  0.000  0.006  0.011  0.136 -0.003 -0.003       
WrdL:WNUM.C  0.000  0.009  0.007 -0.059 -0.003 -0.004  0.656
convergence code: 0
Model failed to converge with max|grad| = 0.00242965 (tol = 0.002, component 1)

Data: data %>% filter(FIXATED)
Models:
model0: FPASSD ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C * 
model0:     WordLength + WordLength + WNUM.C + LogWordFreq + (1 | SUBJ) + 
model0:     (1 | ItemID)
model1: FPASSD ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C * 
model1:     WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid + 
model1:     (1 | SUBJ) + (1 | ItemID)
       Df     AIC     BIC   logLik deviance  Chisq Chi Df Pr(>Chisq)    
model0 10 2803241 2803344 -1401610  2803221                             
model1 11 2802734 2802848 -1401356  2802712 508.95      1  < 2.2e-16 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
