(Intercept) & \textbf{200.84} & (5.73) & \textbf{232.71} & (7.84) & \textbf{243.36} & (8.34) \\
WordLength & \textbf{3.54} & (0.23) & \textbf{32.82} & (0.43) & \textbf{35.54} & (0.48) \\
LogWordFreq & \textbf{-4.66} & (0.23) & \textbf{-2.99} & (0.41) & \textbf{-4.92} & (0.45) \\
PositionText & \textbf{-0.65} & (0.18) & \textbf{-2.3} & (0.32) & \textbf{-2.91} & (0.35) \\
WordLength:LogWordFreq & \textbf{0.37} & (0.17) & -0.62 & (0.32) & \textbf{-1.52} & (0.35) \\
LogWordFreq:PositionText & \textbf{-0.54} & (0.22) & \textbf{-0.96} & (0.40) & -0.31 & (0.45) \\
WordLength:PositionText & \textbf{-0.78} & (0.22) & \textbf{-2.94} & (0.41) & \textbf{-2.74} & (0.46) \\
\hline NEAT Surprisal  & \textbf{1.54} & (0.08) & \textbf{3.48} & (0.14) & \textbf{4.21} & (0.15) \\
\quad AIC Difference & 411.0 &  & 633.0 &  & 749.0 &  \\
\quad BIC Difference & 400.0 &  & 623.0 &  & 738.0 &  \\
\quad Deviance & 413.0 &  & 635.0 &  & 751.0 &  \\
\hline Full Surprisal  & \textbf{1.45} & (0.06) & \textbf{3.2} & (0.12) & \textbf{4.03} & (0.13) \\
\quad AIC Difference & 497.0 &  & 735.0 &  & 947.0 &  \\
\quad BIC Difference & 486.0 &  & 725.0 &  & 937.0 &  \\
\quad Deviance & 499.0 &  & 737.0 &  & 949.0 &  \\
\hline Random Surprisal  & \textbf{1.21} & (0.07) & \textbf{2.65} & (0.13) & \textbf{3.18} & (0.14) \\
\quad AIC Difference & 302.0 &  & 439.0 &  & 513.0 &  \\
\quad BIC Difference & 292.0 &  & 429.0 &  & 503.0 &  \\
\quad Deviance & 304.0 &  & 441.0 &  & 515.0 &  \\
