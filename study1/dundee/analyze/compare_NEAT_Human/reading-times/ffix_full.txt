Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: FFIXDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + (1 | SUBJ) +      (1 | ItemID)
   Data: data %>% filter(FIXATED)

     AIC      BIC   logLik deviance df.resid 
 2580623  2580726 -1290302  2580603   226869 

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-3.0266 -0.5660 -0.0756  0.4567 15.3954 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  277.6   16.66   
 SUBJ     (Intercept)  325.6   18.04   
 Residual             4852.1   69.66   
Number of obs: 226879, groups:  ItemID, 46190; SUBJ, 10

Fixed effects:
                       Estimate Std. Error t value
(Intercept)            200.7539     5.7097  35.160
WordLength               2.8550     0.2405  11.872
LogWordFreq             -5.3473     0.2297 -23.283
WNUM.C                  -0.6266     0.1767  -3.546
WordLength:LogWordFreq   0.2536     0.1785   1.421
LogWordFreq:WNUM.C      -0.2202     0.2278  -0.967
WordLength:WNUM.C       -0.5305     0.2294  -2.313

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C WL:LWF LWF:WN
WordLength   0.003                                   
LogWordFreq  0.003  0.579                            
WNUM.C       0.000  0.002  0.011                     
WrdLngt:LWF  0.017  0.314 -0.098 -0.005              
LgWF:WNUM.C  0.000  0.007  0.013  0.150 -0.003       
WrdL:WNUM.C  0.000  0.010  0.009 -0.080 -0.005  0.648
Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: FFIXDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid +      (1 | SUBJ) + (1 | ItemID)
   Data: data %>% filter(FIXATED)

     AIC      BIC   logLik deviance df.resid 
 2580182  2580296 -1290080  2580160   226868 

Scaled residuals: 
    Min      1Q  Median      3Q     Max 
-3.0635 -0.5662 -0.0752  0.4561 15.4054 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  264.3   16.26   
 SUBJ     (Intercept)  325.7   18.05   
 Residual             4852.6   69.66   
Number of obs: 226879, groups:  ItemID, 46190; SUBJ, 10

Fixed effects:
                        Estimate Std. Error t value
(Intercept)            200.76412    5.71021  35.159
WordLength               2.37481    0.24002   9.894
LogWordFreq             -5.66073    0.22876 -24.746
WNUM.C                  -0.62768    0.17577  -3.571
Surprisal.Resid          1.34612    0.06374  21.117
WordLength:LogWordFreq   0.30192    0.17733   1.703
LogWordFreq:WNUM.C      -0.22371    0.22634  -0.988
WordLength:WNUM.C       -0.54180    0.22785  -2.378

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C Srpr.R WL:LWF LWF:WN
WordLength   0.003                                          
LogWordFreq  0.003  0.580                                   
WNUM.C       0.000  0.002  0.011                            
Surprsl.Rsd  0.000 -0.096 -0.065  0.000                     
WrdLngt:LWF  0.017  0.311 -0.100 -0.005  0.013              
LgWF:WNUM.C  0.000  0.007  0.013  0.151 -0.001 -0.003       
WrdL:WNUM.C  0.000  0.010  0.009 -0.081 -0.003 -0.005  0.648
