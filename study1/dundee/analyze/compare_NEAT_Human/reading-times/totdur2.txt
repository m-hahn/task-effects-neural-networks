Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: TOTDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + (1 | SUBJ) +      (1 | ItemID)
   Data: data %>% filter(TOTDUR > 0)

     AIC      BIC   logLik deviance df.resid 
 3367742  3367847 -1683861  3367722   265704 

Scaled residuals: 
   Min     1Q Median     3Q    Max 
-4.700 -0.551 -0.171  0.314 39.866 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  2037.6   45.14  
 SUBJ     (Intercept)   693.7   26.34  
 Residual             17135.1  130.90  
Number of obs: 265714, groups:  ItemID, 46812; SUBJ, 10

Fixed effects:
                       Estimate Std. Error t value
(Intercept)            243.3599     8.3386  29.185
WordLength              35.5424     0.4752  74.796
LogWordFreq             -4.9201     0.4534 -10.851
WNUM.C                  -2.9067     0.3464  -8.392
WordLength:LogWordFreq  -1.5224     0.3519  -4.326
LogWordFreq:WNUM.C      -0.3097     0.4476  -0.692
WordLength:WNUM.C       -2.7385     0.4564  -6.000

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C WL:LWF LWF:WN
WordLength   0.005                                   
LogWordFreq  0.002  0.589                            
WNUM.C       0.000  0.004  0.015                     
WrdLngt:LWF  0.024  0.281 -0.112 -0.005              
LgWF:WNUM.C  0.001  0.005  0.004  0.110  0.003       
WrdL:WNUM.C  0.000  0.010  0.004 -0.043  0.004  0.648
Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: TOTDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid +      (1 | SUBJ) + (1 | ItemID)
   Data: data %>% filter(TOTDUR > 0)

     AIC      BIC   logLik deviance df.resid 
 3366993  3367108 -1683485  3366971   265703 

Scaled residuals: 
   Min     1Q Median     3Q    Max 
-4.630 -0.551 -0.170  0.316 39.835 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  1955.0   44.22  
 SUBJ     (Intercept)   694.4   26.35  
 Residual             17133.1  130.89  
Number of obs: 265714, groups:  ItemID, 46812; SUBJ, 10

Fixed effects:
                       Estimate Std. Error t value
(Intercept)            243.1649     8.3424  29.148
WordLength              33.4566     0.4770  70.140
LogWordFreq             -6.2713     0.4521 -13.872
WNUM.C                  -2.8850     0.3435  -8.398
Surprisal.Resid          4.3939     0.1595  27.548
WordLength:LogWordFreq  -1.5136     0.3487  -4.341
LogWordFreq:WNUM.C      -0.3455     0.4436  -0.779
WordLength:WNUM.C       -2.7596     0.4522  -6.103

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C Srpr.R WL:LWF LWF:WN
WordLength   0.005                                          
LogWordFreq  0.002  0.594                                   
WNUM.C       0.000  0.004  0.015                            
Surprsl.Rsd -0.001 -0.161 -0.110  0.002                     
WrdLngt:LWF  0.024  0.278 -0.113 -0.005  0.001              
LgWF:WNUM.C  0.001  0.005  0.004  0.111 -0.003  0.003       
WrdL:WNUM.C  0.000  0.010  0.004 -0.044 -0.002  0.004  0.648
Data: data %>% filter(TOTDUR > 0)
Models:
model0: TOTDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C * 
model0:     WordLength + WordLength + WNUM.C + LogWordFreq + (1 | SUBJ) + 
model0:     (1 | ItemID)
model1: TOTDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C * 
model1:     WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid + 
model1:     (1 | SUBJ) + (1 | ItemID)
       Df     AIC     BIC   logLik deviance  Chisq Chi Df Pr(>Chisq)    
model0 10 3367742 3367847 -1683861  3367722                             
model1 11 3366993 3367108 -1683485  3366971 751.63      1  < 2.2e-16 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
