def getModel(x):
    x = [[z for z in y.split(" ") if len(z) > 0] for y in x.split("\n")]
    assert x[2][0] == "(Intercept)"
    effects = {z[0] : [float(u) for u in z[1:]] for z in x[2:] if len(z) > 1}
    return effects

def getModelComp(x):
    x = [[z for z in y.split(" ") if len(z) > 0] for y in x]
    return {x[0][i] : float(x[1][i]) for i in range(len(x[0]))}

results = {}
for measure in ["ffix", "fpassd", "totdur"]:
 for suffix in ["_full", "_random", ""]:
     with open(f"{measure}{suffix}.txt", "r") as inFile:
         data = inFile.read().strip()
         data = data[data.index("AIC"):]
         modelComp1 = data.split("\n")[:2]
         data = data[data.index("Fixed effects:"):]
         base_model1 = data[:data.index("Correlation of Fixed")]
         data2 = data[data.index("Formula:"):]
         data2 = data2[data2.index("AIC"):]
         modelComp2 = data2.split("\n")[:2]
         data2 = data2[data2.index("Fixed effects:"):]
         surp_model2 = data2[:data2.index("Correlation of Fixed")]
         base_model = getModel(base_model1)
         surp_model = getModel(surp_model2)
         base_modelComp = getModelComp(modelComp1)
         surp_modelComp = getModelComp(modelComp2)
         base_model["Surprisal.Resid"] = surp_model["Surprisal.Resid"]
         aicDiff = base_modelComp["AIC"] - surp_modelComp["AIC"]
         bicDiff = base_modelComp["BIC"] - surp_modelComp["BIC"]
         devianceDiff = base_modelComp["deviance"] - surp_modelComp["deviance"]
         results[(measure, suffix)] = (base_model, aicDiff, bicDiff, devianceDiff)

def formatResult(x):
    estimate = round(x[0], 2)
    se = round(x[1], 2)
    t = round(x[2], 2)
    return f"{estimate} & ({se}) & {t}"

def format(x):
    x = x.replace(".C", "")
    x = x.replace("WNUM", "PositionText")
    return x
with open(f"output/{__file__}.txt", "w") as outFile:
  for predictor in results[("totdur", "")][0]:
      if predictor == "Surprisal.Resid":
          continue
      line = format(predictor)
      for measure in ["ffix", "fpassd", "totdur"]:
          line += " & "+formatResult(results[(measure, "")][0][predictor])
      line += " \\\\"
      print(line, file=outFile)
  
  for suffix in ["", "_full", "_random"]:
      predictor = "Surprisal.Resid"
      line = "\\hline "+{"" : "NEAT", "_full" : "Full", "_random" : "Random"}[suffix]+" Surprisal "
      for measure in ["ffix", "fpassd", "totdur"]:
          line += " & "+formatResult(results[(measure, suffix)][0][predictor])
      line += " \\\\"
      print(line, file=outFile)
      comparisons = ["AIC Difference", "BIC Difference", "Deviance"]
      for i in range(len(comparisons)):
         line = "\quad "+comparisons[i]
         for measure in ["ffix", "fpassd", "totdur"]:
             line += " & "+str(results[(measure, suffix)][i+1])+" & &"
         line += " \\\\"
         print(line, file=outFile)
  
