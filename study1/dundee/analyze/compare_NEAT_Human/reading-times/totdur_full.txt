Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: TOTDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + (1 | SUBJ) +      (1 | ItemID)
   Data: data %>% filter(TOTDUR > 0)

     AIC      BIC   logLik deviance df.resid 
 3414895  3415000 -1707438  3414875   269596 

Scaled residuals: 
   Min     1Q Median     3Q    Max 
-4.730 -0.552 -0.172  0.315 40.025 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  2012.8   44.86  
 SUBJ     (Intercept)   680.2   26.08  
 Residual             17003.1  130.40  
Number of obs: 269606, groups:  ItemID, 47384; SUBJ, 10

Fixed effects:
                       Estimate Std. Error t value
(Intercept)            244.0121     8.2575  29.550
WordLength              34.8524     0.4847  71.909
LogWordFreq             -5.5716     0.4563 -12.211
WNUM.C                  -2.8907     0.3427  -8.434
WordLength:LogWordFreq  -0.1392     0.3580  -0.389
LogWordFreq:WNUM.C       0.2100     0.4551   0.461
WordLength:WNUM.C       -2.2262     0.4644  -4.794

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C WL:LWF LWF:WN
WordLength   0.006                                   
LogWordFreq  0.004  0.617                            
WNUM.C       0.000  0.003  0.014                     
WrdLngt:LWF  0.026  0.305 -0.044 -0.007              
LgWF:WNUM.C  0.000  0.004  0.011  0.120 -0.003       
WrdL:WNUM.C  0.000  0.006  0.005 -0.032 -0.004  0.665
convergence code: 0
Model failed to converge with max|grad| = 0.00207227 (tol = 0.002, component 1)

Linear mixed model fit by maximum likelihood  ['lmerMod']
Formula: TOTDUR ~ WordLength * LogWordFreq + WNUM.C * LogWordFreq + WNUM.C *      WordLength + WordLength + WNUM.C + LogWordFreq + Surprisal.Resid +      (1 | SUBJ) + (1 | ItemID)
   Data: data %>% filter(TOTDUR > 0)

     AIC      BIC   logLik deviance df.resid 
 3414104  3414220 -1707041  3414082   269595 

Scaled residuals: 
   Min     1Q Median     3Q    Max 
-4.678 -0.553 -0.171  0.316 39.959 

Random effects:
 Groups   Name        Variance Std.Dev.
 ItemID   (Intercept)  1927.1   43.90  
 SUBJ     (Intercept)   681.5   26.11  
 Residual             17001.3  130.39  
Number of obs: 269606, groups:  ItemID, 47384; SUBJ, 10

Fixed effects:
                        Estimate Std. Error t value
(Intercept)            244.03139    8.26512  29.525
WordLength              33.59312    0.48212  69.678
LogWordFreq             -6.42843    0.45305 -14.189
WNUM.C                  -2.89084    0.33980  -8.507
Surprisal.Resid          3.62397    0.12804  28.304
WordLength:LogWordFreq   0.02179    0.35462   0.061
LogWordFreq:WNUM.C       0.20246    0.45080   0.449
WordLength:WNUM.C       -2.24404    0.45987  -4.880

Correlation of Fixed Effects:
            (Intr) WrdLng LgWrdF WNUM.C Srpr.R WL:LWF LWF:WN
WordLength   0.006                                          
LogWordFreq  0.004  0.618                                   
WNUM.C       0.000  0.003  0.014                            
Surprsl.Rsd  0.000 -0.094 -0.068  0.000                     
WrdLngt:LWF  0.025  0.302 -0.047 -0.007  0.015              
LgWF:WNUM.C  0.000  0.004  0.011  0.121 -0.001 -0.003       
WrdL:WNUM.C  0.000  0.006  0.005 -0.033 -0.001 -0.004  0.664
