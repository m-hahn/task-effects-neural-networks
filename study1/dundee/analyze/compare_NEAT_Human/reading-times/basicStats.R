library(tidyr)
library(dplyr)

human = read.csv("BASE/STUDY1/Dundee/ReadingAndPOS.csv", sep="\t")
data = read.csv("BASE/STUDY1/Dundee-annotated/run_lm_noised_dundee.py_MERGED_86850892", sep="\t") %>% rename(PositionModel=Position)

wordfreq = read.csv("../../../../python/vocabularies/cnn_nonAnonymized_50000.txt", sep="\t", header=FALSE, quote=NULL)
names(wordfreq) = c("WordID", "Token", "WordFreq")

data = merge(data, wordfreq, by=c("Token"), all.x=TRUE)

data = data %>% filter(Surprisal_OOV_Status == "InVocab") %>% group_by(Itemno, WNUM) %>% summarise(WordFreq=min(WordFreq), Surprisal=sum(Surprisal))
data$LogWordFreq = log(data$WordFreq)

data = merge(human, data, by=c("Itemno", "WNUM"), all=TRUE)

sink("output/nonExcludedData.txt")
print(mean(is.na(data$FPASSD) | is.na(data$WordFreq)))
sink()
