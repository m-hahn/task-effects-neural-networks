library(tidyr)
library(dplyr)


human = read.csv("BASE/STUDY1/Dundee/ReadingAndPOS.csv", sep="\t")
MODEL = "MERGED"
data = read.csv(paste("BASE/STUDY1/Dundee-annotated/run_attention4_dundee.py_", MODEL, sep=""), sep="\t")

#data = data %>% filter(!(Token %in% c(".", ",", "!", "?")))
#data = data %>% filter(!(Token %in% c("n't", "'d", "'s", 'll', 're', 'clock')))

data2 = data[(2:nrow(data)),]
data3 = data[(1:nrow(data)-1),]
unique(data2[data2$WNUM == data3$WNUM,]$Token)

data2$DUPLICATED = FALSE
duplicated = data2[data2$WNUM == data3$WNUM,]
data2[data2$WNUM == data3$WNUM,]$DUPLICATED = TRUE

data$DUPLICATED = c(FALSE, data2$DUPLICATED)

data = data %>% filter(!DUPLICATED)

rows = data %>% group_by(PositionModel, Token, Itemno, WNUM, ID, InVocab) %>% summarise(rows=NROW(Attended))
print(paste("ASSERT", max(rows$rows) == min(rows$rows), "\n"))

data = data %>% group_by(PositionModel, Token, Itemno, WNUM, ID, InVocab) %>% summarise(AttProbability = mean(AttProbability), Attended = mean(Attended))


surprisal = read.csv("BASE/STUDY1/Dundee-annotated/run_lm_dundee.py_42636007", sep="\t", header=FALSE)
names(surprisal) = c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab", "Surprisal")
data = merge(data, surprisal, by=c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab"))



wordfreq = read.csv("vocabularies/cnn_nonAnonymized_50000.txt", sep="\t", header=FALSE, quote=NULL)
names(wordfreq) = c("WordID", "Token", "WordFreq")

data = merge(data, wordfreq, by=c("Token"))

data = data %>% filter(InVocab == "InVocab") %>% group_by(Itemno, WNUM) %>% summarise(AttProbability = max(AttProbability), WordFreq=min(WordFreq), Surprisal=sum(Surprisal))
data$LogWordFreq = log(data$WordFreq)

data = merge(human, data, by=c("Itemno", "WNUM"), all=TRUE)
data$WordLength = unlist(lapply(as.character(data$WORD), FUN=nchar))
library(lme4)
data = data %>% mutate(ItemID = paste0(Itemno, "_", WNUM))
data$FIXATED = (data$FPASSD > 0)

#summary(lmer(FPASSD ~ WordLength + WNUM + LogWordFreq + AttProbability + Surprisal + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED)))

data = data %>% mutate(WordLength = (WordLength-mean(WordLength, na.rm=TRUE))/sd(WordLength, na.rm=TRUE))
data = data %>% mutate(LogWordFreq = (LogWordFreq-mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
data = data %>% mutate(WNUM.C = (WNUM-mean(WNUM, na.rm=TRUE))/sd(WNUM, na.rm=TRUE))
data = data %>% mutate(AttProbability = (AttProbability-mean(AttProbability, na.rm=TRUE))/sd(AttProbability, na.rm=TRUE))
data = data %>% mutate(Surprisal = (Surprisal-mean(Surprisal, na.rm=TRUE))/sd(Surprisal, na.rm=TRUE))


data$Surprisal.Resid = residuals(lm(Surprisal ~ LogWordFreq, data=data, na.action=na.exclude))
data$AttProbability.Resid = residuals(lm(AttProbability ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + Surprisal, data=data, na.action=na.exclude))



library(brms)
#
#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 10000), family="bernoulli", cores=4))   
#
#
#                       Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                  0.42      0.21    -0.02     0.86        677 1.00
#WordLength                 0.99      0.09     0.81     1.16        962 1.00
#LogWordFreq               -0.28      0.03    -0.35    -0.21       1836 1.00
#WNUM.C                    -0.01      0.03    -0.06     0.05       1883 1.00
#AttProbability.Resid       0.05      0.04    -0.02     0.12       2984 1.00
#Surprisal.Resid            0.00      0.02    -0.04     0.04       3020 1.00
#WordLength:LogWordFreq     0.22      0.05     0.13     0.31       1731 1.00


#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 20000), family="bernoulli", cores=4))   

#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 20000), family="bernoulli", cores=4, iter=5000))   
#   Data: data %>% filter(as.numeric(as.factor(data$ItemID)) (Number of observations: 159952) 


#5000 iterations
#                       Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                  0.41      0.22    -0.02     0.84       1955 1.00
#WordLength                 0.97      0.09     0.79     1.14       2379 1.00
#LogWordFreq               -0.27      0.04    -0.35    -0.19       3911 1.00
#WNUM.C                    -0.01      0.02    -0.05     0.03       4106 1.00
#AttProbability.Resid       0.05      0.03    -0.01     0.10       8109 1.00
#Surprisal.Resid            0.01      0.03    -0.05     0.06       5304 1.00
#WordLength:LogWordFreq     0.26      0.04     0.17     0.34       5118 1.00

########################

#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 20000), family="bernoulli", cores=4))

#
#                       Estimate Est.Error l-95% CI u-95% CI Eff.Sample Rhat
#Intercept                  0.41      0.22    -0.03     0.88        570 1.00
#WordLength                 0.97      0.08     0.80     1.14       1011 1.00
#LogWordFreq               -0.27      0.04    -0.35    -0.19       1311 1.00
#WNUM.C                    -0.01      0.02    -0.05     0.03       1419 1.01
#AttProbability.Resid       0.05      0.03    -0.01     0.10       2169 1.00
#Surprisal.Resid            0.01      0.03    -0.05     0.06       1892 1.00
#WordLength:LogWordFreq     0.25      0.04     0.17     0.34       1489 1.00



#model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid|SUBJ) + (1|ItemID), data=data %>% filter(as.numeric(as.factor(data$ItemID)) < 40000), family="bernoulli", cores=4))

model = (brm(FIXATED ~ WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid + (1+WordLength*LogWordFreq + WordLength + WNUM.C + LogWordFreq + AttProbability.Resid + Surprisal.Resid|SUBJ) + (1|ItemID), data=data, family="bernoulli", cores=4))

