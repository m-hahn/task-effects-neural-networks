library(tidyr)
library(dplyr)


human = read.csv("BASE/STUDY1/Dundee/ReadingAndPOS.csv", sep="\t")
MODEL = "MERGED"
data = read.csv(paste("BASE/STUDY1/Dundee-annotated/run_attention4_dundee.py_", MODEL, sep=""), sep="\t")

#data = data %>% filter(!(Token %in% c(".", ",", "!", "?")))
#data = data %>% filter(!(Token %in% c("n't", "'d", "'s", 'll', 're', 'clock')))

data2 = data[(2:nrow(data)),]
data3 = data[(1:nrow(data)-1),]
unique(data2[data2$WNUM == data3$WNUM,]$Token)

data2$DUPLICATED = FALSE
duplicated = data2[data2$WNUM == data3$WNUM,]
data2[data2$WNUM == data3$WNUM,]$DUPLICATED = TRUE

data$DUPLICATED = c(FALSE, data2$DUPLICATED)

data = data %>% filter(!DUPLICATED)

rows = data %>% group_by(PositionModel, Token, Itemno, WNUM, ID, InVocab) %>% summarise(rows=NROW(Attended))
print(paste("ASSERT", max(rows$rows) == min(rows$rows), "\n"))

data = data %>% group_by(PositionModel, Token, Itemno, WNUM, ID, InVocab) %>% summarise(AttProbability = mean(AttProbability), Attended = mean(Attended))


surprisal = read.csv("BASE/STUDY1/Dundee-annotated/run_lm_dundee.py_42636007", sep="\t", header=FALSE)
names(surprisal) = c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab", "Surprisal")
data = merge(data, surprisal, by=c("PositionModel", "Token", "Itemno", "WNUM", "ID", "InVocab"))



wordfreq = read.csv("vocabularies/cnn_nonAnonymized_50000.txt", sep="\t", header=FALSE, quote=NULL)
names(wordfreq) = c("WordID", "Token", "WordFreq")

data = merge(data, wordfreq, by=c("Token"))

data = data %>% filter(InVocab == "InVocab") %>% group_by(Itemno, WNUM) %>% summarise(AttProbability = max(AttProbability), WordFreq=min(WordFreq), Surprisal=sum(Surprisal))
data$LogWordFreq = log(data$WordFreq)

data = merge(human, data, by=c("Itemno", "WNUM"), all=TRUE)
data$WordLength = unlist(lapply(as.character(data$WORD), FUN=nchar))
library(lme4)
data = data %>% mutate(ItemID = paste0(Itemno, "_", WNUM))
data$FIXATED = (data$FPASSD > 0)

#summary(lmer(FPASSD ~ WordLength + WNUM + LogWordFreq + AttProbability + Surprisal + (1|SUBJ) + (1|ItemID), data=data %>% filter(FIXATED)))

data = data %>% mutate(WordLength.C = (WordLength-mean(WordLength, na.rm=TRUE))/sd(WordLength, na.rm=TRUE))
data = data %>% mutate(LogWordFreq.C = (LogWordFreq-mean(LogWordFreq, na.rm=TRUE))/sd(LogWordFreq, na.rm=TRUE))
data = data %>% mutate(WNUM.C = (WNUM-mean(WNUM, na.rm=TRUE))/sd(WNUM, na.rm=TRUE))
data = data %>% mutate(AttProbability.C = (AttProbability-mean(AttProbability, na.rm=TRUE))/sd(AttProbability, na.rm=TRUE))
data = data %>% mutate(Surprisal.C = (Surprisal-mean(Surprisal, na.rm=TRUE))/sd(Surprisal, na.rm=TRUE))


data$Surprisal.Resid = residuals(lm(Surprisal ~ LogWordFreq, data=data, na.action=na.exclude))
data$AttProbability.Resid = residuals(lm(AttProbability ~ WordLength*LogWordFreq + WordLength + WNUM + LogWordFreq + Surprisal, data=data, na.action=na.exclude))


cross_entropy = data %>% summarise(cross_entropy = -mean(FIXATED * log(AttProbability) + (1-FIXATED) * log(1-AttProbability), na.rm=TRUE))

full_data = data %>% group_by(WNUM, WORD) %>% summarise(FixationRate=mean(FIXATED, na.rm=TRUE), AttProbability=mean(AttProbability, na.rm=TRUE))
cor.test(full_data$FixationRate, full_data$AttProbability)

# Heatmap
heatmap = data %>% filter(Itemno == 3, WNUM < 100) %>% group_by(WNUM, WORD) %>% summarise(FixationRate=mean(FIXATED, na.rm=TRUE), AttProbability=mean(AttProbability, na.rm=TRUE))
low = 0
high=1
                                                                                                                             
                cat("\\definecolor{none}{rgb}{0.99,0.99,0.99}","\n",sep="")     
for(i in (1:99)) {      
   colorID = min(18, max(3, floor((heatmap$FixationRate[i] - low) * 20 / (high - low))))                                                                                   
   cat(" \\colorbox{color", colorID,     "}{",as.character(heatmap$WORD[i]),"}",sep="")                                                                                       
   if(i %% 10 == 0) {                                                                                                                                                                  
           cat("\n")                                                                                                                                                                   
   }                      
}
