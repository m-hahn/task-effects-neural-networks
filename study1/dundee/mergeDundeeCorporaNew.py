


# This file merges the Dundee Treebank (dundeegoldann_allreaders.csv) with Vera Demberg's version with excluded data (corpus+gopast)

DEMBERG_CORPUS = "/u/scr/mhahn/Dundee/corpus+gopast"
TREEBANK = "/u/scr/mhahn/Dundee/TheDundeeTreebank_v1-0.csv"
ENRICHED_TREEBANK = "/u/scr/mhahn/Dundee/DundeeMerged.csv"
TEXT_ONLY = "/u/scr/mhahn/Dundee/DundeeTreebankWithWords.csv"
READING_MEASURES_AND_POS = "/u/scr/mhahn/Dundee/ReadingAndPOS.csv"
TOKENIZED = "/u/scr/mhahn/Dundee/DundeeTreebankTokenized.csv"

dembergCorpus = open(DEMBERG_CORPUS, 'r')


from collections import defaultdict
corpusEntries = defaultdict(dict)
headerCorpus = ""
counter = 0
for line in dembergCorpus:
   line = line.rstrip()
   if len(line) < 10:
      continue
   if counter == 0:
      headerCorpus = line.split("\t")
      headerCorpusList = headerCorpus
      headerCorpus = dict(list(zip(headerCorpus, range(len(headerCorpus)))))
   else:
      lineList = line.split("\t")
      fileNum = lineList[headerCorpus["FILE"]]
      subjName = lineList[headerCorpus["SUBJ"]]
      wnum = lineList[headerCorpus["WNUM"]]
      if fileNum.startswith("0"):
           fileNum = fileNum[1:]
      key = (fileNum, subjName, wnum)
      #print(key)
      corpusEntries[(fileNum, wnum)][subjName] = lineList
   counter = counter+1



dembergCorpus.close()

treebank = open(TREEBANK, 'r')

noCorrespondingThing = 0
success = 0

used = {}

defaultString = "\t".join(["NA"] * len(headerCorpus))




enrichedTreebankList = []
counter = -1
for line in treebank:
    counter += 1
    line = line.rstrip()
    if len(line) < 10:
      continue
    if counter == 0:
      headerTreebank = line.split("\t")
      headerTreebankList = headerTreebank
#      enrichedTreebank.write()
      headerTreebank = dict(list(zip(headerTreebank, range(len(headerTreebank)))))

      jointHeaderList = headerTreebankList+headerCorpusList
      print(jointHeaderList)
      jointHeader = dict(list(zip(jointHeaderList, range(len(jointHeaderList)))))
      
    else:
      lineList = line.split("\t")
      fileNum = lineList[headerTreebank["Itemno"]]
      wnum = lineList[headerTreebank["WNUM"]]
      SUBJECTS = "sa sb sc sd se sf sg sh si sj".split(" ")
      key = (fileNum, wnum)
      for subject in SUBJECTS:
            if key not in corpusEntries:
                corpusLine = defaultString.split("\t")
                corpusLine[headerCorpus["WORD"]] = "UNKNOWN"
                corpusLine[headerCorpus["SUBJ"]] = subject
            elif subject in corpusEntries[key]:
                corpusLine= corpusEntries[key][subject]
            else:
                word = [x[headerCorpus["WORD"]] for _, x in corpusEntries[key].items()]
                word = [x for x in word if x != "NA"][0]
                wnum = [x[headerCorpus["WNUM"]] for _, x in corpusEntries[key].items()]
                wnum = [x for x in wnum if x != "NA"][0]
                corpusLine = defaultString.split("\t")
                corpusLine[headerCorpus["WNUM"]] = wnum
                corpusLine[headerCorpus["WORD"]] = word
                corpusLine[headerCorpus["SUBJ"]] = subject
            overallLine = lineList+corpusLine
            overallLine[jointHeader["WNUM"]] = wnum
            assert overallLine[jointHeader["SUBJ"]] != "NA", overallLine
            assert overallLine[jointHeader["Itemno"]] != "NA", overallLine
            assert overallLine[jointHeader["WNUM"]] != "NA", overallLine
            enrichedTreebankList.append(overallLine)
      #print(len((line+corpusLine).split("\t")))

for x in enrichedTreebankList:
     try:
         _ = (x[jointHeader["SUBJ"]], int(x[jointHeader["Itemno"]]), int(x[jointHeader["WNUM"]]))
     except ValueError:
        print(x)
        quit()
enrichedTreebankList = sorted(enrichedTreebankList, key=lambda x:(x[jointHeader["SUBJ"]], int(x[jointHeader["Itemno"]]), int(x[jointHeader["WNUM"]])))
#quit()
with open(ENRICHED_TREEBANK, 'w') as outFile:
   print("\t".join(jointHeaderList), file=outFile)
   for line in enrichedTreebankList:
      print("\t".join(line), file=outFile)
headerTextOnly = ["Itemno", "WNUM", "SentenceID", "ID", "WORD", "CPOS", "Head", "DepRel"]
with open(TEXT_ONLY, 'w') as outFile:
    print("\t".join(headerTextOnly), file=outFile)
    for line in enrichedTreebankList:
        if line[jointHeader["SUBJ"]] == "sa":
            print("\t".join([line[jointHeader[x]] for x in headerTextOnly]), file=outFile)

headerReadingAndPOS = ["Itemno", "WNUM", "SentenceID", "SUBJ", "WORD", "CPOS", "TOTDUR", "FFIXDUR", "FPASSD"]
key = lambda x:(x[jointHeader["Itemno"]], x[jointHeader["SUBJ"]], x[jointHeader["WNUM"]])
lastKey = None
with open(READING_MEASURES_AND_POS, 'w') as outFile:
  print("\t".join(headerReadingAndPOS), file=outFile)
  for i in range(len(enrichedTreebankList)):
    key_ = key(enrichedTreebankList[i])
    if key_ == lastKey:
       continue
    lastKey = key_
    print("\t".join([enrichedTreebankList[i][jointHeader[x]] for x in headerReadingAndPOS]), file=outFile)



key = lambda x:(x[jointHeader["Itemno"]], x[jointHeader["WNUM"]])
lastKey = None
headerTokenized = ["Itemno", "WNUM", "SentenceID", "ID", "WORD"]
with open(TOKENIZED, 'w') as outFile:
   print("\t".join(headerTokenized + ["Token"]), file=outFile)
   for i_ in range(len(enrichedTreebankList)):
    if enrichedTreebankList[i_][jointHeader["SUBJ"]] != "sa":
      continue
    key_ = key(enrichedTreebankList[i_])
    if key_ == lastKey:
       continue
    lastKey = key_
    word = enrichedTreebankList[i_][jointHeader["WORD"]].lower()
    ords = [ord(x) for x in word]
    inRange = lambda x : (x >= 97 and x <= 122) or (x >= 48 and x <= 57) or x == 45
    transitions = [i for i in range(len(word)) if not inRange(ords[i])] + [len(word)+1]
    if len(transitions) == 1:
       tokens = [word]
    else:
       tokens = [word[:transitions[0]]]
       for I in range(len(transitions)-1):
           tokens.append(word[transitions[I]:transitions[I+1]])
       tokens = [x.replace("_", "'") for x in tokens if len(x) > 0]
       for I in range(1, len(tokens)):
         if tokens[I] == "'t" and tokens[I-1].endswith("n"):
           tokens[I] = "n't"
           tokens[I-1] = tokens[I-1][:-1]
#       print(tokens)
    for tok in tokens:
       print("\t".join([enrichedTreebankList[i_][jointHeader[x]] for x in headerTokenized] + [tok]), file=outFile)

