import glob

SCRIPT = __file__.replace("collectLogs_", "")


logs = glob.glob(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/study1Scores_2020/accuracy_{SCRIPT}_*.txt")
print(logs)
with open(f"logs/{__file__}.tsv", "w") as outFile:
 print("\t".join(["ModelID", "alpha", "FixationRate", "Epochs"]), file=outFile)
 for f in logs:
  with open(f, "r") as inFile:
    data = inFile.read().strip().split("\n")
 # print(data, len(data))
  args, devAccuracies, devLosses, devRewards, fixationRates, rewards = data
  args = dict([x.split("=") for x in args[10:-1].split(", ")])
#  print(args["LAMBDA"])
  fixationRate = rewards.split("\t")[2].strip().split(" ")[0]
#  print(devRewards)
  print("\t".join([args["myID"], args["LAMBDA"], fixationRate, str(len(devRewards.split(", "))) ]), file=outFile)
