# Train Attention Module

* [Collect logs without context](collectLogs_train_attention4_CharPreview.py), [with context](collectLogs_train_attention4_CharPreview_WithContext.py)
* [Run all models on Dundee, without context](run_attention4_CharPreview_dundee_all.py), [with context](run_attention4_CharPreview_WithContext_dundee_all.py)
* [Train without context](train_attention4_CharPreview.py), [with context](train_attention4_CharPreview_WithContext.py)
* [Collect predictions without context](united_attention4_CharPreview_dundee.py), [with context](united_attention4_CharPreview_WithContext_dundee.py)

