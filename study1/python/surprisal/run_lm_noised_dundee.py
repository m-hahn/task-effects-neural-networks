# train_attention_6.py: THIS one works nice and fast

import random
import argparse
import glob

parser = argparse.ArgumentParser()


parser.add_argument('--corpus', type=str, default="cnn")
parser.add_argument('--batchSize', type=int, default=64) #random.choice([16, 32]))
parser.add_argument('--learning_rate', type=float, default=random.choice([1.0]))
parser.add_argument('--glove', type=bool, default=True)
parser.add_argument('--dropout', type=float, default=random.choice([0.0, 0.05, 0.1, 0.15, 0.2]))
parser.add_argument('--myID', type=int, default=random.randint(1000,100000000))
parser.add_argument('--SEQUENCE_LENGTH', type=int, default=50)
#parser.add_argument('--LOAD_CKPT', type=str, default="65591293") #random.choice([1.5, 1.75, 2, 2.25, 2.5]))
parser.add_argument('--LOAD_CKPT', type=str, default="86850892") #random.choice([1.5, 1.75, 2, 2.25, 2.5]))
parser.add_argument('--fixation_rate', type=float, default=0.64)
parser.add_argument("--NEAT_model", type=str, default=None)
args = parser.parse_args()

SEQUENCE_LENGTH = args.SEQUENCE_LENGTH

vocabulary = [x.split("\t") for x in open(f"vocabularies/{args.corpus}_nonAnonymized.txt", "r").read().strip().split("\n")]
itos = [x[1] for x in vocabulary]
stoi = dict([(x[1], int(x[0])) for x in vocabulary])

def unique(l):
    s = set()
    r = []
    for x in l:
      if x not in s:
        s.add(x)
        r.append(x)
    return r

answerDistribution = [0, [0 for _ in range(600)]]
 

import gzip
import os

HEADER = ["Position", "Token", "Itemno", "WNUM", "ID", "InVocab", "Reconstruction", "Attention", "Attended"]
HEADER = dict([(HEADER[i], i) for i in range(len(HEADER))])
def loadQACorpus(corpus, partition, batchSize, permuteEntities=True):
        print(args.NEAT_model)
        with open(glob.glob(f"/u/scr/mhahn/Dundee-annotated/run_attention4_CharPreview*_dundee.py_{args.NEAT_model}")[0], "r") as inFile:
           data = [x.split("\t") for x in inFile.read().strip().split("\n")]
        concatenated = data
        partitions = []
        for i in range(int(len(concatenated)/SEQUENCE_LENGTH)+1):
          r = concatenated[i*SEQUENCE_LENGTH:(i+1)*SEQUENCE_LENGTH]
          if len(r) > 0:
            partitions.append(r)
        for i in range(int(len(partitions)/args.batchSize)+1):
          r = partitions[i*args.batchSize:(i+1)*args.batchSize]
          if len(r) > 0:
            yield r
OOV = 2
SKIPPED = 1
PAD = 0
PLACEHOLDER = 3

#training = 


def numerify(token):
   if token == "@placeholder":
      return PLACEHOLDER
   elif token not in stoi or stoi[token] >= 50000:
      return OOV
   else:
      return stoi[token]+4

import torch





word_embeddings = torch.nn.Embedding(num_embeddings = 50000+4, embedding_dim = 200).cuda()


########################
reader = torch.nn.LSTM(200, 1024, 1).cuda()
#reconstructor = torch.nn.LSTM(200, 1024, 1).cuda()
output = torch.nn.Linear(1024, 50000 + 4).cuda()

input_dropout = torch.nn.Dropout(args.dropout)

nllLoss = torch.nn.NLLLoss(reduction="none", ignore_index=PAD)
crossEntropy = torch.nn.CrossEntropyLoss(reduction="none", ignore_index=PAD)

components_lm = [word_embeddings, reader, output]




def parameters():
 for c in components_lm:
   for param in c.parameters():
      yield param

state = torch.load(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2020/train_lm_noised.py_{args.LOAD_CKPT}.ckpt")

print("args", state["args"])
for i in range(len(components_lm)):
   components_lm[i].load_state_dict(state["components"][i])




def forward(batch, calculateAccuracy=False):
    texts = [[PAD] + [numerify(y[HEADER["Token"]]) for y in x] + [PAD] for x in batch] # [:500]
    text_length = max([len(x) for x in texts])
    for text in texts:
       while len(text) < text_length:
          text.append(PAD)
    texts =  torch.LongTensor(texts).cuda()

#    mask = torch.FloatTensor([1 for _ in range(len(batch))]).cuda()
    masked = torch.LongTensor([SKIPPED]).cuda().unsqueeze(1).expand(len(batch), texts.size()[1]-1)
#    print(masked.size())
    hidden = None
    outputs = []


    mask = torch.bernoulli(torch.FloatTensor([[args.fixation_rate for _ in range(texts.size()[0])] for _ in range(texts.size()[1]-1)])).transpose(0,1)
    print(mask.size(), texts.size(), len(batch[0]))
    for batch_ in range(texts.size()[0]):
     for pos in range(1, texts.size()[1]-1):
      try:
        lineForWord = batch[batch_][pos-1]
#        print(batch_, texts.size(), mask.size(), pos, texts[batch_, pos], lineForWord)
        mask[batch_, pos] = 1 if float(lineForWord[HEADER["Attended"]]) == 1 else 0
#        text_from_batch.append([str(y) for  y in [lineForWord[HEADER["Position"]], lineForWord[HEADER["Token"]], lineForWord[HEADER["Itemno"]], lineForWord[HEADER["WNUM"]], lineForWord[HEADER["ID"]], "InVocab" if stoi.get(lineForWord[HEADER["Token"]], 100000) < 50000 else "OOV"] +[round(float(x),4) for x in [loss_reader[pos,batch_]]]])
  #  print(mask.mean())
 #   quit() 
      except IndexError:
        assert pos > len(batch[batch_])
        continue
    mask = mask.cuda()
#    print(mask.size(), texts.size(), masked.size())
    embedded_ = word_embeddings(torch.where(mask==1.0, texts[:,:-1], masked)).transpose(0,1)
    outputs_reader, hidden = reader(embedded_)

#    embedded = word_embeddings(texts).transpose(0,1)
 #   if not calculateAccuracy:
  #     embedded = input_dropout(embedded)
#    hidden = (hidden[0].unsqueeze(0), hidden[1].unsqueeze(0))
#    outputs_decoder, _ = reconstructor(embedded[:-1], hidden)
    targets = texts.transpose(0,1).contiguous()
    targets = targets[1:]
    outputs_cat = output(outputs_reader) # dim=0))
    loss = crossEntropy(outputs_cat.view(-1, 50004), targets.view(-1)).view(outputs_cat.size()[0], outputs_cat.size()[1])

    text_from_batch = []


    if True:
       sequenceLengthHere= text_length-2
       loss_reader = loss.cpu()
       for batch_ in range(loss.size()[1]):
        for pos in range(loss.size()[0]):
         try:
#           print(batch[batch_][pos])
           lineForWord = batch[batch_][pos]
           text_from_batch.append([str(y) for  y in [lineForWord[HEADER["Position"]], lineForWord[HEADER["Token"]], lineForWord[HEADER["Itemno"]], lineForWord[HEADER["WNUM"]], lineForWord[HEADER["ID"]], "InVocab" if stoi.get(lineForWord[HEADER["Token"]], 100000) < 50000 else "OOV"] +[round(float(x),4) for x in [loss_reader[pos,batch_]]]])
         except IndexError:
           pass
    loss = loss.mean(dim=0)
    print("return", len(text_from_batch))
    return loss, text_from_batch

args.clip_type = random.choice([2, "inf", "None"])
args.clip_bound = random.choice([2, 5, 10, 15])

fixationRunningAverageByCondition = [0.5,0.5]
lossAverageByCondition = [10.0, 10.0]

import time


learning_rate=args.learning_rate

devLosses = []
lossRunningAverage = 6.4
noImprovement = 0

concatenated = []
with open(f"/u/scr/mhahn/Dundee-annotated/{__file__}_{args.NEAT_model}_{args.LOAD_CKPT}", "w") as outFile:
    print("\t".join(["Position", "Token", "Itemno", "WNUM", "ID", "Surprisal_OOV_Status", "Surprisal"]), file=outFile)
    validLoss = []
    examplesNumber = 0
    TEXT_ = []
    batches = list(loadQACorpus(args.corpus, None, args.batchSize, permuteEntities=True))
    print("batches", len(batches))
    for batch in batches:
     with torch.no_grad():
       loss, TEXT = forward(batch, calculateAccuracy = True)
       loss = float(loss.mean())
       print("VALID", loss, examplesNumber)
       for x in TEXT:
         TEXT_.append(x)

     validLoss.append(float(loss)*len(batch))
     examplesNumber += len(batch)
     count = 0
    for x in TEXT_:
       print("\t".join(x), file=outFile)

