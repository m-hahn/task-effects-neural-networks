import glob
files_ = glob.glob("/u/scr/mhahn/NEURAL_ATTENTION_TASK/checkpoints_2021/train_attention4_CharPreview*.py_*.ckpt")
import subprocess
files = []
for f in files_:
   print(f)
   id_ = f[f.rfind("_")+1:-5]
   fs = glob.glob(f"/u/scr/mhahn/NEURAL_ATTENTION_TASK/study1Scores_2020/accuracy_*_{id_}.txt")
   if len(fs) == 0:
     continue
   print(fs)
   with open(fs[0], "r") as inFile:
     args = dict([x.split("=") for x in next(inFile).strip().replace("Namespace(", "").rstrip(")").split(", ")])
     print(args["LAMBDA"])
     if float(args["LAMBDA"]) != 3:
       continue
     next(inFile)
     accuracies = next(inFile).strip().split(", ")
     print(accuracies)
     if len(accuracies)  == 1:
      continue
   subprocess.call(["/u/nlp/anaconda/main/anaconda3/envs/py37-mhahn/bin/python", "run_lm_noised_dundee.py", "--NEAT_model="+id_])
for id_ in ["FULL", "RANDOM"]:
   print(id_)
   subprocess.call(["/u/nlp/anaconda/main/anaconda3/envs/py37-mhahn/bin/python", "run_lm_noised_dundee_fullOrRandom.py", "--NEAT_model="+id_])

