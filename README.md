# Modeling Task Effects in Reading with Neural Attention

## Study 1

* [Train Decoder](study1/python/autoencoder)
* [Train Attention](study1/python/attention) 
* [Surprisal Model](study1/python/surprisal) 
* [Accuracies](study1/dundee/analyze/compare_NEAT_Human/accuracy)
* [Reading Time Evaluation](study1/dundee/analyze/compare_NEAT_Human/reading-times)

## Experiment 1

* [Descriptive statistics](study2/analysis/analysis/basic_stats/replication_descriptive_2.R) 
* [Reading times on the answer](study2/analysis/analysis/basic_stats/replication_answer_scores.R), [results](study2/analysis/analysis/basic_stats/output/correct.txt) and [results](study2/analysis/analysis/basic_stats/output/correct_model.txt)  
* [Mixed Effects Models](study2/analysis/analysis/mixed_models). See README there for details. 
* [Heatmaps](study2/analysis/analysis_with_NEAT/heatmaps) 

## Study 2

* [Pretrain QA Model](study2/NEAT/pretrain_qa) 
* [Train NEAT](study2/NEAT/train_NEAT) 
* [Mixed Effects Model](study2/analysis/analysis_with_NEAT/analyzing_neat/analyze2021_Cap_Cond_Char2_TrainEmb_MarkNE_Softmax_LengthResid_Surprisal2.R), [output](study2/analysis/analysis_with_NEAT/analyzing_neat/outputs/finalModel.txt)
* [Heatmaps](study2/analysis/analysis_with_NEAT/heatmaps) 

## Data

This repository contains the processed eye-tracking data collected and analyzed in Experiment 1. 

Data from previous work, i.e. the DeepMind and Dundee corpora, are bundled in a directory expected to be mounted at address BASE. As we do not have permission to republish those, they are not included in this repository. Please contact the authors if you need assistance with access to those files.

